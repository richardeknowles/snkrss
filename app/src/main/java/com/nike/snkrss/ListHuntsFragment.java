package com.nike.snkrss;

/*
 * Created by richardknowles on 6/21/17.
 */

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.constants.GlobalConstants;
import com.nike.buriedtreasure.managers.BuriedTreasureManager;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.models.HuntList;
import com.nike.buriedtreasure.models.HuntMetaData;
import com.nike.buriedtreasure.utilities.HuntUtils;
import com.nike.buriedtreasure.utilities.NikeHttpClient;
import com.nike.buriedtreasure.utilities.Utility;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.ACTIVITY_SERVICE;

public class ListHuntsFragment extends Fragment {

    private Unbinder unbinder;

    @BindView( R2.id.hunts_container )
    LinearLayout huntsContainer;

    @BindView( R2.id.progress )
    View progress;

    @BindView( R2.id.input_upmid )
    EditText idInput;

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ) {
        View view = inflater.inflate( R.layout.fragment_list_hunts, container, false );

        unbinder = ButterKnife.bind( this, view );

        return view;
    }

    @Override
    public void onActivityCreated( @Nullable Bundle savedInstanceState ) {
        super.onActivityCreated( savedInstanceState );

        BuriedTreasureManager buriedTreasureManager = BuriedTreasureManager.getInstance( getActivity() );

        buriedTreasureManager.setAuthToken( GlobalConstants.TEMP_AUTHTOKEN );

        String upmid = HuntUtils.getUpMid( getActivity() );

        refreshHunts( upmid );

        idInput.setText( upmid );
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if ( unbinder != null ) {
            unbinder.unbind();
        }
    }

    @OnClick( R2.id.clear_data )
    public void onClearDataClick() {
        ( (ActivityManager) getActivity().getSystemService( ACTIVITY_SERVICE ) )
                .clearApplicationUserData();
    }

    @OnClick( R2.id.set_mid )
    public void onSetMidClick() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences( GlobalConstants.UPMID_PREFS, Context.MODE_PRIVATE );

        SharedPreferences.Editor editor = sharedPreferences.edit();

        Editable s = idInput.getText();

        String upmid = s != null && s.length() > 1 ? s.toString() : GlobalConstants.DEFAULT_UPMID;

        editor.putString( GlobalConstants.UP_MID_KEY, upmid );

        editor.apply();

        refreshHunts( upmid );
    }

    private void refreshHunts( String upmid ) {
        progress.setVisibility( View.VISIBLE );

        huntsContainer.removeAllViews();

        NikeHttpClient.getApi().getHunts( upmid, BuriedTreasureManager.getInstance( getActivity() ).getBearerAuthToken() ).enqueue( new Callback<HuntList>() {
            @Override
            public void onResponse( @NonNull Call<HuntList> call, @NonNull Response<HuntList> response ) {
                if ( isAdded() ) {
                    progress.setVisibility( View.GONE );

                    if ( response.isSuccessful() ) {
                        HuntList huntList = response.body();

                        final BuriedTreasureManager buriedTreasureManager = BuriedTreasureManager.getInstance( getActivity() );

                        if ( huntList != null ) {
                            for ( final Hunt hunt : huntList.getHunts() ) {
                                if ( hunt != null && GlobalConstants.HUNT_TYPE_TO_DISPLAY.equals( hunt.getType() ) ) {
                                    buriedTreasureManager.saveHuntData( hunt );

                                    Button button = new Button( getActivity() );

                                    HuntMetaData huntMetaData = hunt.getHuntMetaData();

                                    button.setText( huntMetaData != null && !Utility.isNullOrEmpty( huntMetaData.getTheme() )
                                            ? huntMetaData.getTheme() : hunt.id );

                                    button.setOnClickListener( new View.OnClickListener() {
                                        @Override
                                        public void onClick( View v ) {
                                            if ( buriedTreasureManager.hasHuntData( hunt.id ) ) {
                                                Hunt huntData = buriedTreasureManager.getHuntData( hunt.id );
                                                startActivity( buriedTreasureManager.getBuriedTreasureIntent( getContext(), huntData ) );
                                            } else {
                                                Toast.makeText( getActivity(), "Missing hunt data. Must fetch the data from network.", Toast.LENGTH_SHORT ).show();
                                            }
                                        }
                                    } );

                                    huntsContainer.addView( button );
                                }
                            }
                        }
                    } else {
                        Toast.makeText( getActivity(), "Response failed", Toast.LENGTH_SHORT ).show();

                        loadFromJson();
                    }
                }
            }

            @Override
            public void onFailure( @NonNull Call<HuntList> call, @NonNull Throwable t ) {
                if ( isAdded() ) {
                    progress.setVisibility( View.GONE );

                    Toast.makeText( getActivity(), "Response Failure", Toast.LENGTH_SHORT ).show();

                    loadFromJson();
                }
            }
        } );
    }

    private void loadFromJson() {
        final BuriedTreasureManager buriedTreasureManager = BuriedTreasureManager.getInstance( getActivity() );

        try {
            InputStream inputStream = getActivity().getAssets().open( "mock_list.json" );

            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( inputStream ) );

            String line = bufferedReader.readLine();

            StringBuilder lineBuilder = new StringBuilder();

            while ( line != null ) {
                lineBuilder.append( line );

                line = bufferedReader.readLine();
            }

            inputStream.close();

            HuntList huntList = new Gson().fromJson( lineBuilder.toString(), HuntList.class );

            if ( huntList != null ) {
                for ( final Hunt hunt : huntList.getHunts() ) {
                    buriedTreasureManager.saveHuntData( hunt );

                    Button button = new Button( getActivity() );

                    HuntMetaData huntMetaData = hunt.getHuntMetaData();

                    button.setText( huntMetaData != null && !Utility.isNullOrEmpty( huntMetaData.getTheme() )
                            ? huntMetaData.getTheme() : hunt.id );

                    button.setOnClickListener( new View.OnClickListener() {
                        @Override
                        public void onClick( View v ) {
                            if ( buriedTreasureManager.hasHuntData( hunt.id ) ) {
                                Hunt huntData = buriedTreasureManager.getHuntData( hunt.id );
                                startActivity( buriedTreasureManager.getBuriedTreasureIntent( getContext(), huntData ) );
                            } else {
                                Toast.makeText( getActivity(), "Missing hunt data. Must fetch the data from network.", Toast.LENGTH_SHORT ).show();
                            }
                        }
                    } );

                    huntsContainer.addView( button );
                }
            }

        } catch (Exception e) {
            Log.d( "ListHuntsFragment", e.getMessage() );
        }
    }
}
