package com.nike.buriedtreasure.managers;

/*
 * Created by richardknowles on 7/30/17.
 */

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.activities.StashActivity;
import com.nike.buriedtreasure.constants.GlobalConstants;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.utilities.Utility;

public class BuriedTreasureManager {

    @SuppressLint("StaticFieldLeak")
    private static BuriedTreasureManager sInstance;

    private static final Object lock = new Object();

    private Context context;

    private String authToken;

    private BuriedTreasureManager( Context context ) {
        super();

        this.context = context.getApplicationContext();
    }

    public static BuriedTreasureManager getInstance( Context context ) {
        if ( sInstance == null ) {
            synchronized ( lock ) {
                if ( sInstance == null ) {
                    sInstance = new BuriedTreasureManager( context );
                }
            }
        }

        return sInstance;
    }

    public boolean hasHuntData( @NonNull String huntId ) {
        return !Utility.isNullOrEmpty( getHuntDataPrefs().getString( huntId, null ) );
    }

    public Hunt getHuntData( @NonNull String huntId ) {
        Gson gson = new Gson();

        String huntJson = getHuntDataPrefs().getString( huntId, null );

        return !Utility.isNullOrEmpty( huntJson ) ? gson.fromJson( huntJson, Hunt.class ) : null;
    }

    public void saveHuntData( @NonNull Hunt hunt ) {
        Gson gson = new Gson();

        String huntJson = gson.toJson( hunt );

        SharedPreferences.Editor editor = getHuntDataPrefs().edit();

        editor.putString( hunt.id, huntJson );

        editor.apply();
    }

    public String getAuthToken() {
        return authToken;
    }

    public String getBearerAuthToken() {
        return context.getString( R.string.bearer_auth_token, authToken );
    }

    public void setAuthToken( String authToken ) {
        this.authToken = authToken;
    }

    public Intent getBuriedTreasureIntent( @NonNull Context context, @NonNull Hunt hunt ) {
        Intent intent = new Intent( context, StashActivity.class );

        intent.putExtra( StashActivity.HUNT_DATA_EXTRA, hunt );

        return intent;
    }

    public Intent getProductThreadIntent( @NonNull Context context, @NonNull String threadId, @NonNull String campaignId ) {
        Intent intent = new Intent();

        String packageName = context.getPackageName();

        intent.setComponent( new ComponentName( packageName, TextUtils.concat( packageName, ".MainActivity" ).toString() ) );

        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );

        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK )
                .addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );

        intent.putExtra( GlobalConstants.THREAD_ID_EXTRA, threadId )
                .putExtra( GlobalConstants.CAMPAIGN_ID_EXTRA, campaignId );

        return intent;
    }

    private SharedPreferences getHuntDataPrefs() {
        return context.getSharedPreferences( GlobalConstants.HUNT_DATA_PREFS, Context.MODE_PRIVATE );
    }
}
