package com.nike.buriedtreasure.tasks;

/*
 * Created by richardknowles on 7/9/17.
 */

import android.os.AsyncTask;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;

public class ImageDownloaderTask extends AsyncTask<String, File, ImageDownloaderTask.Result> {
    public static final String IMAGES_DOWNLOADED_FILTER_ACTION = "com.nike.buriedtreasure.IMAGE_DOWNLOADER";

    private static final String TAG = "ImageDownloaderTask";

    private static final int TIMEOUT = 10;

    private final RequestManager glide;

    private Listener listener;

    public ImageDownloaderTask( RequestManager glide, Listener listener ) {
        this.glide = glide;

        this.listener = listener;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Result doInBackground( String... params ) {
        FutureTarget<File>[] requests = new FutureTarget[ params.length ];
        for ( int i = 0; i < params.length; i++ ) {
            if ( isCancelled() ) {
                break;
            }

            requests[ i ] = glide
                    .load( params[ i ] )
                    .downloadOnly( Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL );
        }

        Result result = new Result();
        for ( int i = 0; i < params.length; i++ ) {
            if ( isCancelled() ) {
                for ( int j = i; j < params.length; j++ ) {
                    if ( requests[ i ] != null ) Glide.clear( requests[ i ] );

                    result.failures.put( params[ j ], new CancellationException() );
                }

                break;
            }
            try {
                File file = requests[ i ].get( TIMEOUT, TimeUnit.SECONDS );

                result.success.put( params[ i ], file );

                publishProgress( file );
            } catch ( Exception e ) {
                result.failures.put( params[ i ], e );
            } finally {
                Glide.clear( requests[ i ] );
            }
        }

        return result;
    }

    @Override
    protected void onProgressUpdate( File... values ) {
        for ( File file : values ) {
            Log.v( TAG, "Finished " + file );
        }
    }

    @Override
    protected void onPostExecute( Result result ) {
        if ( listener != null ) {
            listener.onResult( result );
        }

        Log.i( TAG, String.format( Locale.ROOT, "Downloaded %d files, %d failed.",
                result.success.size(), result.failures.size() ) );
    }

    public class Result {
        Map<String, File> success = new HashMap<>();

        Map<String, Exception> failures = new HashMap<>();
    }

    public interface Listener {
        void onResult( Result result );
    }
}
