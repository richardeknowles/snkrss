package com.nike.buriedtreasure.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/*
 * Created by richardknowles on 6/10/17.
 */

public class OnboardingFragment extends Fragment implements ViewPager.OnPageChangeListener {

    public static final String TAG = "OnboardingFragment";

    private static final float CIRCLE_INDICATOR_ALPHA = 0.2f;

    private static final float CIRCLE_INDICATOR_NO_ALPHA = 1.0f;

    private static final int CROSS_FADE_ANIMATION_DURATION = 750;

    private Listener listener;

    private Unbinder unbinder;

    private int previousPosition = 0;

    @BindView( R2.id.view_pager )
    ViewPager viewPager;

    @BindView( R2.id.circle_1 )
    View circle1;

    @BindView( R2.id.circle_2 )
    View circle2;

    @BindView( R2.id.circle_3 )
    View circle3;

    @BindView( R2.id.circle_4 )
    View circle4;

    @BindView( R2.id.circle_5 )
    View circle5;

    @BindView( R2.id.skip_instructions )
    View skipInstructions;

    @BindView( R2.id.get_started )
    View getStarted;

    @Override
    public void onAttach( Context context ) {
        super.onAttach( context );

        try {
            listener = (Listener) context;
        } catch ( ClassCastException e ) {
            throw new ClassCastException( e.toString() );
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        listener = null;
    }

    public static OnboardingFragment newInstance() {
        return new OnboardingFragment();
    }

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ) {
        View view = inflater.inflate( R.layout.fragment_onboarding, container, false );

        unbinder = ButterKnife.bind( this, view );

        resetIndicators();

        circle1.setAlpha( CIRCLE_INDICATOR_NO_ALPHA );

        viewPager.setAdapter( new OnboardingFragmentPagerAdapter( getChildFragmentManager() ) );

        viewPager.addOnPageChangeListener( this );

        viewPager.setOffscreenPageLimit( 4 );

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if ( unbinder != null ) {
            unbinder.unbind();
        }
    }

    @Override
    public void onPageScrolled( int position, float positionOffset, int positionOffsetPixels ) {

    }

    @Override
    public void onPageSelected( int position ) {
        resetIndicators();

        if ( position != 4 ) {
            if ( position == 0 ) {
                circle1.setAlpha( CIRCLE_INDICATOR_NO_ALPHA );
            } else if ( position == 1 ) {
                circle2.setAlpha( CIRCLE_INDICATOR_NO_ALPHA );
            } else if ( position == 2 ) {
                circle3.setAlpha( CIRCLE_INDICATOR_NO_ALPHA );
            } else if ( position == 3 ) {
                circle4.setAlpha( CIRCLE_INDICATOR_NO_ALPHA );
            }

            if ( previousPosition == 4 ) {
                getStarted.setVisibility( View.INVISIBLE );

                skipInstructions.setAlpha( 0f );

                skipInstructions.setVisibility( View.VISIBLE );

                skipInstructions.animate()
                        .alpha( 1f )
                        .setDuration( CROSS_FADE_ANIMATION_DURATION )
                        .setListener( null );
            } else {
                skipInstructions.setVisibility( View.VISIBLE );
            }
        } else {
            circle5.setAlpha( CIRCLE_INDICATOR_NO_ALPHA );

            skipInstructions.setVisibility( View.GONE );

            getStarted.setAlpha( 0f );

            getStarted.setVisibility( View.VISIBLE );

            getStarted.animate()
                    .alpha( 1.0f )
                    .setDuration( CROSS_FADE_ANIMATION_DURATION )
                    .setListener( null );
        }

        previousPosition = position;
    }

    @Override
    public void onPageScrollStateChanged( int state ) {

    }

    private void resetIndicators() {
        circle1.setAlpha( CIRCLE_INDICATOR_ALPHA );

        circle2.setAlpha( CIRCLE_INDICATOR_ALPHA );

        circle3.setAlpha( CIRCLE_INDICATOR_ALPHA );

        circle4.setAlpha( CIRCLE_INDICATOR_ALPHA );

        circle5.setAlpha( CIRCLE_INDICATOR_ALPHA );
    }

    @OnClick({ R2.id.skip_instructions, R2.id.get_started })
    public void onSkipOrGetStartedClick( View view ) {
        if ( listener != null ) {
            listener.onSkipOrGetStartedClick( view );
        }
    }

    private class OnboardingFragmentPagerAdapter extends FragmentPagerAdapter {

        private final List<String> videos = new ArrayList<>();

        private Fragment[] fragments;

        OnboardingFragmentPagerAdapter( FragmentManager manager ) {
            super( manager );

            videos.add( "snkrs_stash_01" );

            videos.add( "snkrs_stash_02" );

            videos.add( "snkrs_stash_03" );

            videos.add( "snkrs_stash_04" );

            videos.add( "snkrs_stash_05" );

            fragments = new Fragment[ getCount() ];
        }

        @Override
        public Fragment getItem( int position ) {
            if ( fragments != null && position >= 0 && fragments.length > position && fragments[ position ] != null ) {
                return fragments[ position ];
            }

            String videoFile = videos.get( position );

            if ( position == 0 ) {
                return OnboardingVideoFragment.newInstance( videoFile, getString( R.string.welcome_to_snkrs_stash ),
                        getString( R.string.explore_your_city ) );
            } else if ( position == 1 ) {
                return OnboardingVideoFragment.newInstance( videoFile, getString( R.string.step_1_choose_a_stash ),
                        getString( R.string.swipe_through_locations ) );
            } else if ( position == 2 ) {
                return OnboardingVideoFragment.newInstance( videoFile, getString( R.string.step_2_follow_the_clue ),
                        getString( R.string.open_stash_spot ) );
            } else if ( position == 3 ) {
                return OnboardingVideoFragment.newInstance( videoFile, getString( R.string.step_3_check_in ),
                        getString( R.string.tap_ship_pair ) );
            } else {
                return OnboardingVideoFragment.newInstance( videoFile, getString( R.string.ready_to_explore ),
                        getString( R.string.find_closest_to_you ) );
            }
        }

        @Override
        public int getCount() {
            return videos.size();
        }
    }

    public interface Listener {
        void onSkipOrGetStartedClick( View view );
    }
}
