package com.nike.buriedtreasure.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.views.NestedWebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class ProductDetailsFragment extends Fragment {

    private static final String HUNT_KEY = "hunt_key";

    private Unbinder unbinder;

    private Listener listener;

    @BindView( R2.id.web_view )
    NestedWebView webView;

    @BindView( R2.id.progress )
    public View progress;

    @BindView( R2.id.arrow )
    public View arrow;

    public ProductDetailsFragment() {}

    public static ProductDetailsFragment newInstance( Hunt hunt ) {
        ProductDetailsFragment productDetailsFragment = new ProductDetailsFragment();

        Bundle bundle = new Bundle();

        bundle.putParcelable( HUNT_KEY, hunt );

        productDetailsFragment.setArguments( bundle );

        return productDetailsFragment;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View view = inflater.inflate( R.layout.fragment_product_details, container, false );

        unbinder = ButterKnife.bind( this, view );

        Hunt hunt = getArguments().getParcelable( HUNT_KEY );

        initWebView();

        webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings( "deprecation" )
            @Override
            public boolean shouldOverrideUrlLoading( WebView view, String url ) {
                view.getContext().startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( url ) ) );

                return true;
            }

            @Override
            public void onPageStarted( WebView view, String url, Bitmap favicon) {
                if (webView != null && progress != null) {
                    webView.setVisibility( View.GONE );

                    progress.setVisibility( View.VISIBLE );
                }

                if (webView != null) {
                    webView.setVisibility( View.GONE );
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if ( webView != null && progress != null ) {
                    webView.setVisibility( View.VISIBLE );

                    progress.setVisibility( View.GONE );
                }

                if ( webView != null ) {
                    webView.setVisibility( View.VISIBLE );
                }
            }
        });


        if ( hunt.getHuntMetaData() != null && !TextUtils.isEmpty( hunt.getHuntMetaData().getDetailsUrl() ) ) {
            webView.loadUrl( hunt.getHuntMetaData().getDetailsUrl() );
        }

        webView.setListener( new NestedWebView.Listener() {
            @Override
            public void onScrollChanged( int l, int t, int oldl, int oldt ) {
                arrow.scrollBy( 0, oldt - t );
            }
        } );

        webView.setOnLongClickListener( new View.OnLongClickListener() {
            @Override
            public boolean onLongClick( View v ) {
                return true;
            }
        } );

        webView.setOnTouchListener( new View.OnTouchListener() {
            @Override
            public boolean onTouch( View v, MotionEvent event ) {
                if ( webView.getScrollY() != 0 ) {
                    v.getParent().requestDisallowInterceptTouchEvent( true );
                } else {
                    v.getParent().requestDisallowInterceptTouchEvent( false );
                }

                //TODO: remove once fix scroll
//                log.setText( Integer.toString( webView.getScrollY() ) + " " + event.toString() );

                return false;
            }
        } );

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if ( unbinder != null ) {
            unbinder.unbind();
        }
    }

    @OnClick( R2.id.arrow )
    public void onArrowClick( View view ) {
        if ( listener != null ) {
            listener.onArrowClick( view );
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        WebSettings settings = webView.getSettings();

        settings.setJavaScriptEnabled( true );

        settings.setAllowContentAccess( false );

        settings.setAllowFileAccess( false );

        settings.setAllowFileAccessFromFileURLs( false );

        settings.setAllowUniversalAccessFromFileURLs( false );
    }

    public void setListener( Listener listener ) {
        this.listener = listener;
    }

    public interface Listener {
        void onArrowClick( View view );
    }
}
