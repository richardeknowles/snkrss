package com.nike.buriedtreasure.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.views.VerticalViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class StashFragment extends Fragment implements StashCarouselFragment.Listener,
        ProductDetailsFragment.Listener {

    public static final String TAG = "StashFragment";

    private static final int STASH_CAROUSEL_POSITION = 0;

    private static final int PRODUCT_DETAILS_POSITION = 1;

    private static final int PAGE_COUNT = 2;

    private Unbinder unbinder;

    private Listener listener;

    private boolean isCarouselLoaded;

    private boolean isBackgroundLoaded;

    private boolean isSneakerImageLoaded;

    private boolean hasImagesDownloaded;

    private boolean hasLandingImagesLoaded;

    @BindView( R2.id.vertical_view_pager )
    VerticalViewPager verticalViewPager;

    @BindView( R2.id.loading_progress )
    View loadingProgress;

    @Override
    public void onAttach( Context context ) {
        super.onAttach( context );

        try {
            listener = (Listener) context;
        } catch ( ClassCastException e ) {
            throw new ClassCastException( e.toString() );
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        listener = null;
    }

    public static StashFragment newInstance() {
        return new StashFragment();
    }

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ) {
        View view = inflater.inflate( R.layout.fragment_stash, container, false );

        unbinder = ButterKnife.bind( this, view );

        Hunt hunt = listener.getHunt();

        VerticalViewPagerAdapter verticalViewPagerAdapter = new VerticalViewPagerAdapter( getFragmentManager(), hunt, this, this );

        verticalViewPager.setAdapter( verticalViewPagerAdapter );

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if ( unbinder != null ) {
            unbinder.unbind();
        }
    }

    @Override
    public void displayProductDetails() {
        verticalViewPager.setCurrentItem( PRODUCT_DETAILS_POSITION, true );
    }

    @Override
    public void onCarouselLayout() {
        isCarouselLoaded = true;

        refreshLoadingVisibility();
    }

    @Override
    public void onBackgroundLoadComplete() {
        isBackgroundLoaded = true;

        refreshLoadingVisibility();
    }

    @Override
    public void onSneakerImageLoadComplete() {
        isSneakerImageLoaded = true;

        refreshLoadingVisibility();
    }

    @Override
    public void onLandingImagesLoaded() {
        hasLandingImagesLoaded = true;

        refreshLoadingVisibility();
    }

    @Override
    public boolean isImageDownloadComplete() {
        return listener != null && listener.isImageDownloadComplete();
    }

    @Override
    public void onHelpCardClick() {
        if ( listener != null ) {
            listener.onHelpCardClick();
        }
    }

    @Override
    public void onImagesDownloaded() {
        hasImagesDownloaded = true;

        refreshLoadingVisibility();
    }

    @Override
    public void onArrowClick( View view ) {
        verticalViewPager.setCurrentItem( STASH_CAROUSEL_POSITION, true );
    }

    private void refreshLoadingVisibility() {
        if ( hasLandingImagesLoaded && hasImagesDownloaded && isSneakerImageLoaded && isBackgroundLoaded && isCarouselLoaded ) {
            loadingProgress.setVisibility( View.GONE );
        } else {
            loadingProgress.setVisibility( View.VISIBLE );
        }
    }

    private static class VerticalViewPagerAdapter extends FragmentStatePagerAdapter {
        private Hunt hunt;

        private StashCarouselFragment.Listener carouselListener;

        private ProductDetailsFragment.Listener productDetailsListener;

        private VerticalViewPagerAdapter( FragmentManager fm, @NonNull Hunt hunt, StashCarouselFragment.Listener carouselListener,
                                          ProductDetailsFragment.Listener productDetailsListener ) {
            super( fm );

            this.hunt = hunt;

            this.carouselListener = carouselListener;

            this.productDetailsListener = productDetailsListener;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem( int position) {
            if ( position == STASH_CAROUSEL_POSITION ) {
                StashCarouselFragment stashCarouselFragment = StashCarouselFragment.newInstance( hunt );

                stashCarouselFragment.setListener( carouselListener );

                return stashCarouselFragment;
            } else {
                ProductDetailsFragment productDetailsFragment = ProductDetailsFragment.newInstance( hunt );

                productDetailsFragment.setListener( productDetailsListener );

                return productDetailsFragment;
            }
        }
    }

    public interface Listener {
        Hunt getHunt();

        boolean isImageDownloadComplete();

        void onHelpCardClick();
    }
}
