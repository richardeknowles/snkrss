package com.nike.buriedtreasure.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.activities.LocationPhotoSphereActivity;
import com.nike.buriedtreasure.activities.StashActivity;
import com.nike.buriedtreasure.models.ColorHint;
import com.nike.buriedtreasure.models.Cover;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.models.HuntMetaData;
import com.nike.buriedtreasure.models.Stash;
import com.nike.buriedtreasure.tasks.ImageDownloaderTask;
import com.nike.buriedtreasure.utilities.HuntUtils;
import com.nike.buriedtreasure.utilities.StylingUtilities;
import com.nike.buriedtreasure.utilities.Utility;
import com.nike.buriedtreasure.views.CarouselView;
import com.nike.buriedtreasure.views.ICarouselItem;

import java.io.File;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.grantland.widget.AutofitHelper;

public class StashCarouselFragment extends Fragment implements ImageDownloaderTask.Listener {

    private static final String HUNT_KEY = "hunt_key";

    private Unbinder unbinder;

    private Hunt hunt;

    private Listener listener;

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive( Context context, Intent intent ) {
            if ( !isAdded() ) {
                return;
            }

            String action = intent != null ? intent.getAction() : null;

            if ( action != null ) {
                switch ( action ) {
                    case ImageDownloaderTask.IMAGES_DOWNLOADED_FILTER_ACTION:
                        updateUI();

                        break;
                    case HuntUtils.HUNT_UPDATED_FILTER_ACTION:
                        Hunt newHunt = intent.getParcelableExtra( StashActivity.HUNT_DATA_EXTRA );

                        if ( hunt != null ) {
                            hunt = newHunt;

                            carouselView.onNewHunt( hunt );
                        }

                        break;
                }
            }
        }
    };

    @BindView(R2.id.carousel_view)
    CarouselView carouselView;

    @BindView(R2.id.background)
    ImageView background;

    @BindView(R2.id.stash_pin)
    ImageView stashPin;

    @BindView(R2.id.snkrs_stash)
    TextView snkrsStash;

    @BindView(R2.id.theme)
    TextView theme;

    @BindView(R2.id.product_details)
    TextView productDetails;

    @BindView(R2.id.down_arrow)
    ImageView downArrow;

    @BindView(R2.id.back_nav)
    ImageView backNav;

    @BindView( R2.id.sneaker )
    ImageView sneaker;

    public StashCarouselFragment() {}

    public static StashCarouselFragment newInstance( Hunt hunt ) {
        StashCarouselFragment stashCarouselFragment = new StashCarouselFragment();

        Bundle bundle = new Bundle();

        bundle.putParcelable( HUNT_KEY, hunt );

        stashCarouselFragment.setArguments( bundle );

        return stashCarouselFragment;
    }

    @Override
    public void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction( ImageDownloaderTask.IMAGES_DOWNLOADED_FILTER_ACTION );

        intentFilter.addAction( HuntUtils.HUNT_UPDATED_FILTER_ACTION );

        LocalBroadcastManager.getInstance( getActivity() ).registerReceiver( broadcastReceiver, intentFilter );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View view = inflater.inflate( R.layout.fragment_stash_carousel, container, false );

        unbinder = ButterKnife.bind( this, view );

        hunt = getArguments().getParcelable( HUNT_KEY );

        return view;
    }

    @OnClick( R2.id.product_details_container )
    public void onProductDetailsClick() {
        if ( listener != null ) {
            listener.displayProductDetails();
        }
    }

    @OnClick( R2.id.back_nav )
    public void onBackNavClick() {
        getActivity().onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();

        if ( listener != null && listener.isImageDownloadComplete() ) {
            updateUI();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if ( unbinder != null ) {
            unbinder.unbind();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if ( isAdded() ) {
            LocalBroadcastManager.getInstance( getActivity() )
                    .unregisterReceiver( broadcastReceiver );
        }
    }

    @Override
    public void onResult( ImageDownloaderTask.Result result ) {
        updateUI();
    }

    public void setListener( Listener listener ) {
        this.listener = listener;
    }

    private void updateUI() {
        if ( isAdded() && hunt != null && hunt.getHuntMetaData() != null && carouselView.size() == 0 ) {
            final HuntMetaData huntMetaData = hunt.getHuntMetaData();

            carouselView.with( hunt );

            ColorHint colorHint = huntMetaData.getColorHint();

            if ( !Utility.isNullOrEmpty( huntMetaData.getBackgroundUrl() ) ) {
                Glide.with( getContext() )
                        .load( huntMetaData.getBackgroundUrl() )
                        .diskCacheStrategy( DiskCacheStrategy.ALL )
                        .into( new GlideDrawableImageViewTarget( background ) {
                            @Override
                            protected void setResource( GlideDrawable resource ) {
                                super.setResource( resource );

                                background.setImageDrawable( resource );

                                if ( listener != null ) {
                                    listener.onBackgroundLoadComplete();
                                }
                            }

                            @Override
                            public void onLoadFailed( Exception e, Drawable errorDrawable ) {
                                super.onLoadFailed( e, errorDrawable );

                                if ( listener != null ) {
                                    listener.onBackgroundLoadComplete();
                                }
                            }
                        } );
            } else {
                background.setBackgroundColor( ContextCompat.getColor( getContext(), android.R.color.black ) );
            }

            theme.setText( huntMetaData.getTheme() );

            AutofitHelper helper = AutofitHelper.create( theme );

            helper.setMaxTextSize( getResources().getDimension( R.dimen.stash_theme_text_size ) );

            if ( colorHint != null ) {
                try {
                    int themeColor = StylingUtilities.getColorFromText( getContext(), colorHint.getTheme() );

                    int detailsColor = StylingUtilities.getColorFromText( getContext(), colorHint.getDetailsBtn() );

                    stashPin.setColorFilter( themeColor );

                    backNav.setColorFilter( detailsColor );

                    snkrsStash.setTextColor( themeColor );

                    theme.setTextColor( themeColor );

                    productDetails.setTextColor( detailsColor );

                    downArrow.setColorFilter( detailsColor );
                } catch ( NumberFormatException ignored ) {}
            }

            if ( huntMetaData.getCover() != null ) {
                Cover cover = huntMetaData.getCover();

                if ( !Utility.isNullOrEmpty( cover.getImageUrl() ) ) {
                    Glide.with( getContext() )
                            .load( huntMetaData.getCover().getImageUrl() )
                            .diskCacheStrategy( DiskCacheStrategy.ALL )
                            .into( new GlideDrawableImageViewTarget( sneaker ) {
                                @Override
                                protected void setResource( GlideDrawable resource ) {
                                    super.setResource( resource );

                                    sneaker.setImageDrawable( resource );

                                    if ( listener != null ) {
                                        listener.onSneakerImageLoadComplete();
                                    }
                                }

                                @Override
                                public void onLoadFailed( Exception e, Drawable errorDrawable ) {
                                    super.onLoadFailed( e, errorDrawable );

                                    if ( listener != null ) {
                                        listener.onSneakerImageLoadComplete();
                                    }
                                }
                            } );
                }
            }

            carouselView.setListener( new CarouselView.Listener() {
                @Override
                public void onItemClick( View view, ICarouselItem carouselItem ) {
                    switch ( carouselItem.getType() ) {
                        case ICarouselItem.LANDING:
                            if ( listener != null ) {
                                listener.displayProductDetails();
                            }
                        case ICarouselItem.LOCATION:
                            if ( carouselItem instanceof Stash ) {
                                Stash stash = (Stash) carouselItem;

                                startActivity( new Intent( getActivity(), LocationPhotoSphereActivity.class )
                                        .putExtra( LocationPhotoSphereActivity.STASH_DATA_EXTRA, stash )
                                        .putExtra( StashActivity.HUNT_DATA_EXTRA, hunt ) );
                            }

                            break;
                        case ICarouselItem.HELP:
                            if ( listener != null ) {
                                listener.onHelpCardClick();
                            }
                    }
                }

                @SuppressWarnings( "ResultOfMethodCallIgnored" )
                @Override
                public void onStashLocationShareClick( Stash stash, Bitmap bitmap ) {
                    try {
                        Context applicationContext = getActivity().getApplicationContext();

                        File dir = new File( applicationContext.getCacheDir(), "images" );

                        dir.mkdir();

                        File file = File.createTempFile( "snkrs", ".png", dir );

                        FileOutputStream out = new FileOutputStream( file );

                        bitmap.compress( Bitmap.CompressFormat.PNG, 100, out );

                        out.close();

                        Uri uri = FileProvider.getUriForFile( applicationContext, applicationContext.getPackageName() + ".fileprovider", file );

                        Intent intent = new Intent( Intent.ACTION_SEND )
                                .addFlags( Intent.FLAG_GRANT_READ_URI_PERMISSION )
                                .setType( getActivity().getContentResolver().getType( uri ) )
                                .putExtra( Intent.EXTRA_STREAM, uri );

                        if ( !Utility.isNullOrEmpty( huntMetaData.getInvite() ) ) {
                            StringBuilder stringBuilder = new StringBuilder( huntMetaData.getInvite() );

                            if ( !Utility.isNullOrEmpty( huntMetaData.getInviteUrl() ) ) {
                                stringBuilder.append( " " );

                                stringBuilder.append( huntMetaData.getInviteUrl() );
                            }

                            intent.putExtra( Intent.EXTRA_TEXT, stringBuilder.toString() );
                        }

                        startActivity( Intent.createChooser( intent, getString( R.string.share_image_chooser ) ) );
                    } catch ( Exception e ) {
                        Toast.makeText( getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT ).show();
                    }
                }

                @Override
                public void onLandingImagesLoaded() {
                    if ( listener != null ) {
                        listener.onLandingImagesLoaded();
                    }
                }

                @Override
                public void onLandingGlobalLayout( float x, float y, int width ) {
                    sneaker.setX( x + width / 2 - getResources().getDimension( R.dimen.carousel_landing_shoe_width ) / 2 );

                    sneaker.setY( y + getResources().getDimension( R.dimen.landing_sneaker_margin_top ) );
                }

                @Override
                public void onScrolled( RecyclerView recyclerView, int dx, int dy ) {
                    if ( dx == 0 && dy == 0 ) {
                        return;
                    }

                    sneaker.setX( sneaker.getX() - (float) ( dx * 1.5 ) );
                }
            } );

            carouselView.getViewTreeObserver().addOnPreDrawListener( new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    if ( listener != null ) {
                        listener.onCarouselLayout();
                    }

                    carouselView.getViewTreeObserver().removeOnPreDrawListener( this );

                    return true;
                }
            } );

            if ( listener != null ) {
                listener.onImagesDownloaded();
            }
        }
    }

    public interface Listener {
        void displayProductDetails();

        void onImagesDownloaded();

        void onCarouselLayout();

        void onBackgroundLoadComplete();

        void onSneakerImageLoadComplete();

        void onLandingImagesLoaded();

        boolean isImageDownloadComplete();

        void onHelpCardClick();
    }
}
