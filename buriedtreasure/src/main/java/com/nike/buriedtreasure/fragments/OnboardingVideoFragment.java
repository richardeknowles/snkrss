package com.nike.buriedtreasure.fragments;

/*
 * Created by richardknowles on 6/18/17.
 */

import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.views.TypefaceTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OnboardingVideoFragment extends Fragment implements TextureView.SurfaceTextureListener {

    private static final String TAG = "OnboardingVideoFragment";

    private static final String VIDEO_RESOURCE_FILENAME = "video_resource_filename";

    private static final String TITLE_KEY = "title_key";

    private static final String BODY_KEY = "body_key";

    private Unbinder unbinder;

    private boolean isFragmentVisible;

    private MediaPlayer mMediaPlayer;

    @BindView(R2.id.video_view)
    TextureView videoView;

    @BindView(R2.id.title)
    TypefaceTextView title;

    @BindView(R2.id.body)
    TypefaceTextView body;

    public static OnboardingVideoFragment newInstance( String fileName, String title, String body ) {
        OnboardingVideoFragment onboardingVideoFragment = new OnboardingVideoFragment();

        Bundle bundle = new Bundle();

        bundle.putString( VIDEO_RESOURCE_FILENAME, fileName );

        bundle.putString( TITLE_KEY, title );

        bundle.putCharSequence( BODY_KEY, body );

        onboardingVideoFragment.setArguments( bundle );

        return onboardingVideoFragment;
    }

    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ) {
        View view = inflater.inflate( R.layout.fragment_onboarding_video, container, false );

        unbinder = ButterKnife.bind( this, view );

        title.setText( getArguments().getString( TITLE_KEY ) );

        body.setText( getArguments().getCharSequence( BODY_KEY ) );

        videoView.setSurfaceTextureListener( this );

        return view;
    }

    @Override
    public void setUserVisibleHint( boolean isVisibleToUser ) {
        super.setUserVisibleHint( isVisibleToUser );

        isFragmentVisible = isVisibleToUser;

        if ( mMediaPlayer != null ) {
            if ( isVisibleToUser ) {
                mMediaPlayer.start();
            } else {
                mMediaPlayer.pause();

                mMediaPlayer.seekTo( 0 );
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if ( unbinder != null ) {
            unbinder.unbind();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if ( mMediaPlayer != null && mMediaPlayer.isPlaying() ) {
            mMediaPlayer.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if ( mMediaPlayer != null && isFragmentVisible && !mMediaPlayer.isPlaying() ) {
            mMediaPlayer.start();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if ( mMediaPlayer != null ) {
            mMediaPlayer.release();

            mMediaPlayer = null;
        }
    }

    @Override
    public void onSurfaceTextureAvailable( SurfaceTexture surfaceTexture, int i, int i2 ) {
        Surface surface = new Surface( surfaceTexture );

        try {
            AssetFileDescriptor afd = getResources().openRawResourceFd( getResources().getIdentifier( getArguments().getString( VIDEO_RESOURCE_FILENAME ), "raw", getActivity().getPackageName() ) );

            mMediaPlayer = new MediaPlayer();

            mMediaPlayer.setDataSource( afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength() );

            mMediaPlayer.setSurface( surface );

            mMediaPlayer.setLooping( true );

            mMediaPlayer.prepareAsync();

            DisplayMetrics displaymetrics = new DisplayMetrics();

            getActivity().getWindowManager().getDefaultDisplay().getMetrics( displaymetrics );

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) videoView.getLayoutParams();

            layoutParams.width = displaymetrics.widthPixels;

            layoutParams.height = displaymetrics.widthPixels;

            videoView.setLayoutParams( layoutParams );

            mMediaPlayer.setOnPreparedListener( new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared( MediaPlayer mediaPlayer ) {
                    mMediaPlayer.seekTo( 0 );

                    if ( isFragmentVisible ) {
                        mMediaPlayer.start();
                    }
                }
            });

        } catch ( Exception e ) {
            Log.d( TAG, e.toString() );
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged( SurfaceTexture surfaceTexture, int i, int i2 ) { }

    @Override
    public boolean onSurfaceTextureDestroyed( SurfaceTexture surfaceTexture ) {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();

            mMediaPlayer.release();

            mMediaPlayer = null;
        }

        return true;
    }

    @Override
    public void onSurfaceTextureUpdated( SurfaceTexture surfaceTexture ) {}

}
