package com.nike.buriedtreasure.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/*
 * Created by richardknowles on 6/22/17.
 */

public class EnableLocationFragment extends Fragment {

    public static final String TAG = "EnableLocationFragment";

    private static final int PERMISSIONS_REQUEST_LOCATION = 1002;

    private Unbinder unbinder;

    private Listener listener;

    @Override
    public void onAttach( Context context ) {
        super.onAttach( context );

        try {
            listener = (Listener) context;
        } catch ( ClassCastException e ) {
            throw new ClassCastException( e.toString() );
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        listener = null;
    }

    public static EnableLocationFragment newInstance() {
        return new EnableLocationFragment();
    }

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ) {
        View view = inflater.inflate( R.layout.fragment_enable_location, container, false );

        unbinder = ButterKnife.bind( this, view );

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if ( unbinder != null ) {
            unbinder.unbind();
        }
    }

    @OnClick( { R2.id.next, R2.id.location_dialog } )
    public void onNextClick( View view ) {
        if ( ActivityCompat.checkSelfPermission( getActivity(), Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            requestPermissions( new String[]{ Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSIONS_REQUEST_LOCATION );
        } else {
            if ( listener != null ) {
                listener.navigateToCarousel();
            }
        }
    }

    @OnClick( R2.id.close )
    public void onCloseClick( View view ) {
        if ( listener != null ) {
            listener.navigateToCarousel();
        }
    }

    @Override
    public void onRequestPermissionsResult( int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                if ( listener != null ) {
                    listener.navigateToCarousel();
                }

                break;
            }
        }
    }

    public interface Listener {
        void navigateToCarousel();
    }
}
