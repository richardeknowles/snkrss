package com.nike.buriedtreasure.helpers;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

/**
 * Allows creating a TypefaceSpan with a custom typeface instead
 * of being limited to the 3 Android system fonts
 * Created by cpaian on 9/2/15.
 * Reference: http://stackoverflow.com/a/17961854/3686125
 */
public class CustomTypefaceSpan extends MetricAffectingSpan {
    private final Typeface mTypeface;
    private Float mTextSize;
    private Integer mColor;

    public CustomTypefaceSpan(final Typeface typeface, Float textSize, Integer color) {
        mTypeface = typeface;
        mTextSize = textSize;
        mColor = color;
    }

    @Override
    public void updateDrawState(final TextPaint drawState) {
        apply(drawState);
    }

    @Override
    public void updateMeasureState(final TextPaint paint) {
        apply(paint);
    }

    private void apply(final Paint paint) {
        final Typeface oldTypeface = paint.getTypeface();
        final int oldStyle = oldTypeface != null ? oldTypeface.getStyle() : 0;
        final int fakeStyle = oldStyle & ~mTypeface.getStyle();

        if ((fakeStyle & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }

        if ((fakeStyle & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }

        if (mTextSize != null) {
            paint.setTextSize(mTextSize);
        }

        if (mColor != null) {
            paint.setColor(mColor);
        }

        paint.setTypeface(mTypeface);
    }
}