package com.nike.buriedtreasure.views;

import  android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.widget.Button;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.utilities.StylingUtilities;


/**
 * Subclass of {@link Button} that supports the <code>customTypeface</code> attribute from XML.
 */
public class TypefaceButton extends AppCompatButton {

    public TypefaceButton(final Context context) {
        this(context, null);
    }

    public TypefaceButton(final Context context, AttributeSet attrs) {
        this(context, attrs, TypefaceButton.useImageButton(context, attrs) ? android.R.attr.imageButtonStyle : android.R.attr.buttonStyle);
    }

    public TypefaceButton(final Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public TypefaceButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        //Can't directly call super with 4th parameter,
        // as this requires API 21 only!
        super(context, attrs, defStyleAttr);

        if (isInEditMode()) return; // loading custom fonts will break the preview engine

        final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TypefaceView);
        if (array != null) {
            final String typefaceAssetPath = array.getString(
                    R.styleable.TypefaceView_typeface);

            if (typefaceAssetPath != null) {
                setTypeface(typefaceAssetPath);
            }

            array.recycle();
        }
    }

    private static boolean useImageButton(Context context, AttributeSet attrs){
        final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TypefaceView);
        if (array != null) {
            final boolean useImgBtn = array.getBoolean(
                    R.styleable.TypefaceView_use_image_button, false);
            array.recycle();
            return useImgBtn;
        }
        return false;
    }

    public void setTypeface(@NonNull String typefaceAssetPath) {
        setTypeface( StylingUtilities.getTypefaceFromAsset(getContext(),typefaceAssetPath));
    }
}
