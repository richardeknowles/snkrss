package com.nike.buriedtreasure.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.nike.buriedtreasure.R;

public class SwipeViewPager extends ViewPager {

	private boolean mPageEnabled = true;

	private boolean mAdjustWrapHeight;
	
	public SwipeViewPager( Context context) {
		super( context );
	}

	public SwipeViewPager( Context context, AttributeSet attrs) {
		super( context, attrs );
		
		TypedArray types = context.obtainStyledAttributes( attrs, R.styleable.SwipeViewPager );

        if ( types != null ) {
            mPageEnabled = types.getBoolean( R.styleable.SwipeViewPager_swipe, true );

			mAdjustWrapHeight = types.getBoolean( R.styleable.SwipeViewPager_wrap, false );

            types.recycle();
        }
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if ( mAdjustWrapHeight ) {
			int height = 0;

			for ( int i = 0; i < getChildCount(); i++ ) {
				View child = getChildAt( i );

				child.measure( widthMeasureSpec, MeasureSpec.makeMeasureSpec( 0, MeasureSpec.UNSPECIFIED ) );

				int childHeight = child.getMeasuredHeight();

				if ( childHeight > height ) height = childHeight;
			}

			heightMeasureSpec = MeasureSpec.makeMeasureSpec( height, MeasureSpec.EXACTLY );
		}

		super.onMeasure( widthMeasureSpec, heightMeasureSpec );
	}
	
	@Override
	public boolean onTouchEvent( MotionEvent event ) {
        return mPageEnabled && super.onTouchEvent(event);
    }
	
	@Override
    public boolean onInterceptTouchEvent( MotionEvent event ) {
        return mPageEnabled && super.onInterceptTouchEvent(event);
    }
}
