package com.nike.buriedtreasure.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.models.ActionMetaData;
import com.nike.buriedtreasure.models.Cover;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.models.HuntMetaData;
import com.nike.buriedtreasure.models.Locations;
import com.nike.buriedtreasure.models.Stash;
import com.nike.buriedtreasure.utilities.HuntUtils;
import com.nike.buriedtreasure.utilities.Utility;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by richardknowles on 3/19/17.
 */

public class CarouselView extends LinearLayout {

    private CarouselAdapter adapter;

    private Listener listener;

    private CarouselLayoutManager carouselLayoutManager;

    @BindView( R2.id.carousel_recycler_view )
    RecyclerView recyclerView;

    public CarouselView( Context context ) {
        this( context, null );
    }

    public CarouselView( Context context, AttributeSet attrs ) {
        this( context, attrs, 0 );
    }

    public CarouselView( final Context context, AttributeSet attrs, int defStyleAttr ) {
        super( context, attrs, defStyleAttr );

        View view = LayoutInflater.from( context ).inflate( R.layout.carousel_view, this, true );

        ButterKnife.bind( this, view );

        boolean isFiniteScroll = false;

        int orientation = LinearLayoutManager.HORIZONTAL;

        TypedArray types = context.obtainStyledAttributes( attrs, R.styleable.CarouselView );

        if ( types != null ) {
            isFiniteScroll = types.getBoolean( R.styleable.CarouselView_finiteScroll, false );

            orientation = types.getInt( R.styleable.CarouselView_orientation, orientation );

            types.recycle();
        }

        adapter = new CarouselAdapter( isFiniteScroll, new CarouselAdapter.Listener() {
            @Override
            public void onItemClick( View view, ICarouselItem carouselImage ) {
                if ( listener != null && carouselImage != null ) {
                    listener.onItemClick( view, carouselImage );
                }
            }

            @Override
            public void onShareClick( Stash stash, Bitmap bitmap ) {
                if ( listener != null && stash != null ) {
                    listener.onStashLocationShareClick( stash, bitmap );
                }
            }

            @Override
            public void onLandingImagesLoaded() {
                if ( listener != null ) {
                    listener.onLandingImagesLoaded();
                }
            }

            @Override
            public void onLandingGlobalLayout( float x, float y , int width) {
                if ( listener != null ) {
                    listener.onLandingGlobalLayout( x, y, width );
                }
            }
        } );

        carouselLayoutManager = new CarouselLayoutManager( context, orientation == 0
                        ? LinearLayoutManager.HORIZONTAL : LinearLayoutManager.VERTICAL, false );

        carouselLayoutManager.setListener( new CarouselLayoutManager.Listener() {
            @Override
            public void onLayoutCompleted( RecyclerView.State state ) {
                if ( carouselLayoutManager.getItemCount() > 0 ) {
                    carouselLayoutManager.setListener( null );
                }
            }
        } );

        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();

        pagerSnapHelper.attachToRecyclerView( recyclerView );

        recyclerView.setNestedScrollingEnabled( false );

        recyclerView.setLayoutManager( carouselLayoutManager );

        recyclerView.setAdapter( adapter );

        final DisplayMetrics displaymetrics = new DisplayMetrics();

        WindowManager windowManager = (WindowManager) getContext().getSystemService( Context.WINDOW_SERVICE );

        windowManager.getDefaultDisplay().getMetrics( displaymetrics );

        recyclerView.addItemDecoration( new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets( Rect outRect, View view, RecyclerView parent, RecyclerView.State state ) {
                super.getItemOffsets( outRect, view, parent, state );

                int offset = (int) ( (displaymetrics.widthPixels - context.getResources().getDimension( R.dimen.carousel_card_width ) ) / 2);

                if ( parent.getChildAdapterPosition( view ) == 0 ) {
                    outRect.left = offset;
                } else if ( parent.getChildAdapterPosition( view ) == parent.getAdapter().getItemCount() - 1 ) {
                    outRect.right = offset;
                }
            }
        } );

        recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled( RecyclerView recyclerView, int dx, int dy ) {
                super.onScrolled( recyclerView, dx, dy );

                if ( listener != null ) {
                    listener.onScrolled( recyclerView, dx, dy );
                }
            }
        } );
    }

    public void setListener( Listener listener ) {
        this.listener = listener;
    }

    private void add( ICarouselItem carouselImage, boolean notify ) {
        adapter.add( carouselImage );

        if ( notify ) {
            adapter.notifyItemInserted( adapter.getCarouselItems().size() - 1 );
        }
    }

    public int size() {
        return adapter.getItemCount();
    }

    public void updateInventoryThreshold( @NonNull Hunt hunt ) {
        adapter.updateInventoryThreshold( hunt );
    }

    public void with( @NonNull Hunt hunt ) {
        List<Stash> stashes = getStashes( hunt );

        HashMap<String, ActionMetaData> foundStashes = HuntUtils.getFoundStashes( hunt );

        Cover cover = hunt.getHuntMetaData() != null ? hunt.getHuntMetaData().getCover() : null;

        if ( cover != null ) {
            add( cover, false );
        }

        if ( !Utility.isNullOrEmpty( stashes ) ) {
            for ( final Stash stash : stashes ) {
                if ( stash != null ) {
                    stash.setHasBeenFound( foundStashes.containsKey( stash.getId() ) );

                    add( stash, false );
                }
            }
        }

        ICarouselItem help = new ICarouselItem() {
            @Override
            public int getType() {
                return ICarouselItem.HELP;
            }
        };

        add( help, false );

        updateInventoryThreshold( hunt );

        notifyDataSetChanged();
    }

    private List<Stash> getStashes( @NonNull Hunt hunt ) {
        HuntMetaData huntMetaData = hunt.getHuntMetaData();

        if ( huntMetaData != null && huntMetaData.getLocations() != null ) {
            Locations locations = huntMetaData.getLocations();

            return locations.getStashes();
        } else {
            return null;
        }
    }

    public void onNewHunt( @NonNull Hunt hunt ) {
        adapter.updateHunt( hunt );
    }

    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    public interface Listener {
        void onItemClick( View view, ICarouselItem carouselItem );

        void onStashLocationShareClick( Stash stash, Bitmap bitmap );

        void onLandingImagesLoaded();

        void onLandingGlobalLayout( float x, float y, int width );

        void onScrolled( RecyclerView recyclerView, int dx, int dy );
    }
}
