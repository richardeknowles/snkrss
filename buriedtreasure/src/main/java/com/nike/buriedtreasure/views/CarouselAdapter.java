package com.nike.buriedtreasure.views;

/*
 * Created by richardknowles on 3/19/17.
 */

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.holders.HelpViewHolder;
import com.nike.buriedtreasure.holders.LandingViewHolder;
import com.nike.buriedtreasure.holders.StashLocationViewHolder;
import com.nike.buriedtreasure.models.ActionMetaData;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.models.HuntMetaData;
import com.nike.buriedtreasure.models.Locations;
import com.nike.buriedtreasure.models.Stash;
import com.nike.buriedtreasure.utilities.HuntUtils;
import com.nike.buriedtreasure.utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CarouselAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    
    private static final int INFINITE_CAROUSEL_LOOP_COUNT = 1000;

    private List<ICarouselItem> carouselItems = new ArrayList<>();

    private boolean isFiniteScroll;

    private double inventoryThreshold;

    private Listener listener;

    public CarouselAdapter( boolean isFiniteScroll, Listener listener ) {
        this.isFiniteScroll = isFiniteScroll;

        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType ) {
        switch ( viewType ) {
            case ICarouselItem.LANDING:
                final LandingViewHolder landingViewHolder =
                        new LandingViewHolder( LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_landing, parent, false ),
                                new LandingViewHolder.Listener() {
                                    @Override
                                    public void onLandingImagesLoaded() {
                                        if ( listener != null ) {
                                            listener.onLandingImagesLoaded();
                                        }
                                    }

                                    @Override
                                    public void onLandingGlobalLayout( float x, float y, int width ) {
                                        if ( listener != null ) {
                                            listener.onLandingGlobalLayout( x, y, width );
                                        }
                                    }
                                } );

                landingViewHolder.itemView.setOnClickListener( getItemViewClickListener( landingViewHolder ) );

                return landingViewHolder;
            case ICarouselItem.LOCATION:
                final StashLocationViewHolder stashLocationViewHolder =
                        new StashLocationViewHolder( LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_stash_location, parent, false ),
                                new StashLocationViewHolder.Listener() {
                                    @Override
                                    public void onShareClick( Stash stash, Bitmap bitmap ) {
                                        if ( listener != null ) {
                                            listener.onShareClick( stash, bitmap );
                                        }
                                    }
                                } );

                stashLocationViewHolder.itemView.setOnClickListener( getItemViewClickListener( stashLocationViewHolder ) );

                return stashLocationViewHolder;
            case ICarouselItem.HELP:
                final HelpViewHolder helpViewHolder =
                        new HelpViewHolder( LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_help, parent, false ) );

                helpViewHolder.itemView.setOnClickListener( getItemViewClickListener( helpViewHolder ) );

                return helpViewHolder;
        }

        return null;
    }

    @Override
    public void onBindViewHolder( final RecyclerView.ViewHolder holder, int position ) {
        ICarouselItem carouselItem = carouselItems.get( position % carouselItems.size() );

        if ( carouselItem != null ) {
            switch ( getItemViewType( position ) ) {
                case ICarouselItem.LANDING:
                    LandingViewHolder landingViewHolder = (LandingViewHolder) holder;

                    landingViewHolder.bind( carouselItem );

                    break;
                case ICarouselItem.LOCATION:
                    StashLocationViewHolder stashLocationViewHolder = (StashLocationViewHolder) holder;

                    stashLocationViewHolder.bind( carouselItem, inventoryThreshold );

                    break;
                case ICarouselItem.HELP:
                    break;
            }
        }
    }


    @Override
    public int getItemCount() {
        return isFiniteScroll ? carouselItems.size() : carouselItems.size() * INFINITE_CAROUSEL_LOOP_COUNT;
    }

    @Override
    public int getItemViewType( int position ) {
        if ( carouselItems != null && position < carouselItems.size() ) {
            ICarouselItem item = carouselItems.get( position );
            
            return item.getType();
        }
        
        return super.getItemViewType( position );
    }

    public void add( ICarouselItem carouselItem ) {
        carouselItems.add( carouselItem );
    }

    public void addAll( List<ICarouselItem> carouselImages ) {
        this.carouselItems.addAll( carouselImages );
    }

    public void clear() {
        carouselItems.clear();
    }

    public void updateInventoryThreshold( Hunt hunt ) {
        if ( hunt != null && hunt.getHuntMetaData() != null ) {
            Locations locations = hunt.getHuntMetaData().getLocations();

            if ( locations != null ) {
                inventoryThreshold = locations.getInventoryThreshold();
            }
        }
    }

    public void updateHunt( @NonNull Hunt hunt ) {
        HuntMetaData huntMetaData = hunt.getHuntMetaData();

        if ( huntMetaData != null && huntMetaData.getLocations() != null ) {
            Locations locations = huntMetaData.getLocations();

            List<Stash> updatedStashes = locations.getStashes();

            HashMap<String, ActionMetaData> foundStashes = HuntUtils.getFoundStashes( hunt );

            if ( !Utility.isNullOrEmpty( updatedStashes ) ) {
                for ( int i = 0; i < getItemCount(); i++ ) {
                    ICarouselItem carouselItem = carouselItems.get( i );

                    if ( carouselItem instanceof Stash ) {
                        Stash stash = (Stash) carouselItem;

                        int index = updatedStashes.indexOf( stash );

                        if ( index > -1 ) {
                            Stash updatedStash = updatedStashes.get( index );

                            boolean shouldNotify = false;

                            if ( !Utility.isNullOrEmpty( stash.getInventory() ) && !Utility.isNullOrEmpty( updatedStash.getInventory() )
                                    && !stash.getInventory().equals( updatedStash.getInventory() ) ) {
                                stash.setInventory( updatedStash.getInventory() );

                                shouldNotify = true;
                            }

                            if ( foundStashes.containsKey( hunt.getId() ) ) {
                                stash.setHasBeenFound( true );

                                shouldNotify = true;
                            }

                            if ( shouldNotify ) {
                                notifyItemChanged( i );
                            }
                        }
                    }
                }
            }
        }
    }

    public List<ICarouselItem> getCarouselItems() {
        return carouselItems;
    }

    private View.OnClickListener getItemViewClickListener( final RecyclerView.ViewHolder holder ) {
        return new View.OnClickListener() {
            @Override
            public void onClick( View view ) {
                if ( !Utility.isNullOrEmpty( carouselItems ) && listener != null && holder.getAdapterPosition() != RecyclerView.NO_POSITION ) {
                    listener.onItemClick( view, carouselItems.get( holder.getAdapterPosition() ) );
                }
            }
        };
    }

    public interface Listener {
        void onItemClick( View view, ICarouselItem carouselItem );

        void onShareClick( Stash stash, Bitmap bitmap );

        void onLandingImagesLoaded();

        void onLandingGlobalLayout( float x, float y, int width );
    }
}
