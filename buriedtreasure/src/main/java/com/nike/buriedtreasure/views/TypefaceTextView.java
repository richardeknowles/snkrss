package com.nike.buriedtreasure.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.utilities.StylingUtilities;


/**
 * Subclass of {@link TextView} that supports the <code>customTypeface</code> attribute from XML.
 */
public class TypefaceTextView extends AppCompatTextView {

    private Context context;

    public TypefaceTextView(final Context context) {
        this(context, null);
    }

    public TypefaceTextView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TypefaceTextView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        this.context = context;

        if (isInEditMode()) return; // loading custom fonts will break the preview engine

        final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TypefaceView);
        if (array != null) {
            final String typefaceAssetPath = array.getString(
                    R.styleable.TypefaceView_typeface);

            if (typefaceAssetPath != null) {
                setTypeface(typefaceAssetPath);
            }
            array.recycle();
        }
    }

    public void setTypeface(@NonNull String typefaceAssetPath) {
        setTypeface( StylingUtilities.getTypefaceFromAsset( context, typefaceAssetPath ) );
    }
}
