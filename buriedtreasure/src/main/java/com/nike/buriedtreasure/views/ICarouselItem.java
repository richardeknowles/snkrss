package com.nike.buriedtreasure.views;

/*
 * Created by richardknowles on 3/24/17.
 */

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface ICarouselItem {

    int LANDING = 0;

    int LOCATION = 1;

    int HELP = 2;

    @IntDef({ LANDING, LOCATION, HELP })
    @Retention( RetentionPolicy.SOURCE )
    @interface CarouselItemType{}

    @ICarouselItem.CarouselItemType int getType();
}
