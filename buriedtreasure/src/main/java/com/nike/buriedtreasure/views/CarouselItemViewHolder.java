package com.nike.buriedtreasure.views;

/*
 * Created by richardknowles on 3/19/17.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.nike.buriedtreasure.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarouselItemViewHolder extends RecyclerView.ViewHolder {

    @BindView( R2.id.image )
    public ImageView imageView;

    public CarouselItemViewHolder( View itemView ) {
        super( itemView );

        ButterKnife.bind( this, itemView );
    }
}
