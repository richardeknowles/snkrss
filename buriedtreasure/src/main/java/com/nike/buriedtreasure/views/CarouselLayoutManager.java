package com.nike.buriedtreasure.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/*
 * Created by richardknowles on 6/3/17.
 */

public class CarouselLayoutManager extends LinearLayoutManager {

    private Listener listener;

    public CarouselLayoutManager( Context context ) {
        super( context );
    }

    public CarouselLayoutManager( Context context, int orientation, boolean reverseLayout ) {
        super( context, orientation, reverseLayout );
    }

    @Override
    public int scrollHorizontallyBy( int dx, RecyclerView.Recycler recycler, RecyclerView.State state ) {
        updateViews();

        return super.scrollHorizontallyBy( dx, recycler, state );
    }

    @Override
    public int scrollVerticallyBy( int dy, RecyclerView.Recycler recycler, RecyclerView.State state ) {
        updateViews();

        return super.scrollVerticallyBy( dy, recycler, state );
    }

    @Override
    public void onLayoutCompleted( RecyclerView.State state ) {
        super.onLayoutCompleted( state );

        if ( listener != null ) {
            listener.onLayoutCompleted( state );
        }

        updateViews();
    }

    public void setListener( Listener listener ) {
        this.listener = listener;
    }

    private void updateViews() {
        for ( int i = 0; i < getChildCount(); i++ ) {
            View child = getChildAt( i );

            float percentage = getOrientation() == HORIZONTAL
                    ? getXPercentageFromCenter( child ) : getYPercentageFromCenter( child );

            float scale = 1f - ( 0.4f * percentage );

            child.setScaleX( scale );

            child.setScaleY( scale );

            child.setAlpha( 1f - percentage );
        }
    }

    private float getXPercentageFromCenter( View child ) {
        float parentCenterX = getWidth() / 2;

        float childCenterX = child.getX() + child.getWidth() / 2;

        float offSet = Math.max( parentCenterX, childCenterX ) - Math.min( parentCenterX, childCenterX );

        int maxOffset = getWidth() / 2 + child.getWidth();

        return offSet / maxOffset;
    }

    private float getYPercentageFromCenter( View child ) {
        float parentCenterY = getHeight() / 2;

        float childCenterY= child.getY() + child.getHeight() / 2;

        float offSet = Math.max( parentCenterY, childCenterY ) - Math.min( parentCenterY, childCenterY );

        int maxOffset = getHeight() / 2 + child.getHeight();

        return offSet / maxOffset;
    }

    public interface Listener {
        void onLayoutCompleted( RecyclerView.State state );
    }
}