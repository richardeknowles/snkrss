package com.nike.buriedtreasure.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;

import butterknife.ButterKnife;
import butterknife.OnClick;

/*
 * Created by richardknowles on 7/9/17.
 */

public class NetworkErrorView extends RelativeLayout {

    private Listener listener;

    public NetworkErrorView(Context context) {
        this( context, null );
    }

    public NetworkErrorView(Context context, AttributeSet attrs) {
        this( context, attrs, -1 );
    }

    public NetworkErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super( context, attrs, defStyleAttr );

        init( context );
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        try {
            listener = (Listener) getContext();
        } catch( ClassCastException e ) {
            throw new ClassCastException( e.toString() );
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        listener = null;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        ButterKnife.bind( this );
    }

    private void init( Context context ) {
        LayoutInflater.from( context ).inflate( R.layout.network_error, this, true );
    }

    @OnClick( R2.id.network_close )
    public void onCloseClick( View view ) {
        if ( listener != null ) {
            listener.onNetworkCloseClick( view );
        }
    }

    @OnClick( R2.id.retry )
    public void onRetryClick( View view ) {
        if ( listener != null ) {
            listener.onNetworkRetryClick( view );
        }
    }

    public interface Listener {
        void onNetworkCloseClick( View view );

        void onNetworkRetryClick( View view );
    }
}

