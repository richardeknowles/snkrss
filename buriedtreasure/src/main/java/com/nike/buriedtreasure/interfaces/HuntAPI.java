package com.nike.buriedtreasure.interfaces;

/*
 * Created by richardknowles on 6/16/17.
 */

import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.models.HuntList;
import com.nike.buriedtreasure.models.ResolvePutBody;
import com.nike.buriedtreasure.models.ResolveResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

public interface HuntAPI {

    String BASE_URL = "https://ecn104-api.nikedev.com/snkrs/";
    String DEBUG_ENVIRONMENT = "0258f796-2b80-11e7-93ae-92361f002671";

    @Headers( { "Cache-Control: no-cache", "Content-Type: application/json" })
    @GET("hunts/v1")
    Call<HuntList> getHunts( @Header("X-NIKE-OVERRIDE-UPMID") String upmid,
                             @Header("Authorization") String authorization );

    @Headers( { "Cache-Control: no-cache", "Content-Type: application/json" })
    @GET("hunts/v1/{id}")
    Observable<Hunt> getHunt( @Header("X-NIKE-OVERRIDE-UPMID") String upmid,
                              @Header("Authorization") String authorization,
                              @Path( "id" ) String huntId );

    @Headers( { "Cache-Control: no-cache", "Content-Type: application/json" })
    @PUT("hunts/v1/{id}/resolve")
    Observable<ResolveResponse> resolveHunt( @Header("X-NIKE-OVERRIDE-UPMID") String upmid,
                                             @Header("Authorization") String authorization,
                                             @Path( "id" ) String huntId,
                                             @Body ResolvePutBody body );
}
