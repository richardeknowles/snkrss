package com.nike.buriedtreasure.holders;

/*
 * Created by richardknowles on 6/19/17.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.models.Stash;
import com.nike.buriedtreasure.utilities.StylingUtilities;
import com.nike.buriedtreasure.utilities.Utility;
import com.nike.buriedtreasure.views.ICarouselItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StashLocationViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = "StashLocationViewHolder";

    private static final float SOLD_OUT_ALPHA = 0.2f;

    private Bitmap bitmap;

    private Stash stash;

    private Listener listener;

    @BindView( R2.id.image )
    public ImageView imageView;

    @BindView( R2.id.overlay )
    public TextView overlay;

    @BindView( R2.id.inventory_progress )
    public ProgressBar inventoryProgress;

    @BindView( R2.id.inventory_percentage )
    public TextView inventoryPercentage;

    @BindView( R2.id.inventory_remaining_title )
    public TextView inventoryRemainingTitle;

    @BindView( R2.id.stash_name )
    public TextView stashName;

    @BindView( R2.id.share_image )
    public ImageView shareImage;

    public StashLocationViewHolder( View itemView, Listener listener ) {
        super( itemView );

        ButterKnife.bind( this, itemView );

        this.listener = listener;
    }

    public void bind( @NonNull ICarouselItem iCarouselItem, double inventoryThreshold ) {
        stash = (Stash) iCarouselItem;

        Context context = imageView.getContext();

        if ( !Utility.isNullOrEmpty( stash.getImageUrl() ) ) {
            int size = imageView.getContext().getResources().getDimensionPixelSize( R.dimen.carousel_card_width );

            Glide.with( context )
                    .load( stash.getImageUrl() )
                    .asBitmap()
                    .centerCrop()
                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                    .into( new SimpleTarget<Bitmap>( size, size ) {
                        @Override
                        public void onResourceReady( Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation ) {
                            bitmap = resource;

                            imageView.setImageBitmap( bitmap );
                        }
                    } );
        }

        overlay.setVisibility( View.GONE );

        inventoryProgress.setProgressDrawable( ContextCompat.getDrawable( context, R.drawable.circular_progress ) );

        inventoryPercentage.setTextColor( ContextCompat.getColor( context, android.R.color.black ) );

        setFooterAlphas( 1 );

        if ( stash.isHasBeenFound() ) {
            overlay.setVisibility( View.VISIBLE );

            overlay.setText( overlay.getContext().getString( R.string.found_em ) );
        }

        String inventory = stash.getInventory();

        if ( !Utility.isNullOrEmpty( inventory ) ) {
            try {
                int percent = Integer.parseInt( inventory );

                inventoryProgress.setProgress( percent );

                inventoryPercentage.setText( TextUtils.concat( inventory, "%" ) );

                if ( percent <= inventoryThreshold ) {
                    inventoryProgress.setProgressDrawable( ContextCompat.getDrawable( context, R.drawable.circular_progress_low_inventory ) );

                    inventoryPercentage.setTextColor( ContextCompat.getColor( context, R.color.red ) );
                }

                if ( percent == 0 ) {
                    inventoryProgress.setProgressDrawable( ContextCompat.getDrawable( context, R.drawable.circular_progress_sold_out ) );

                    inventoryPercentage.setTextColor( ContextCompat.getColor( context, android.R.color.black ) );

                    overlay.setVisibility( View.VISIBLE );

                    if ( !stash.isHasBeenFound() ) {
                        overlay.setText( overlay.getContext().getString( R.string.sold_out ) );
                    }

                    setFooterAlphas( SOLD_OUT_ALPHA );
                } else {
                    setFooterAlphas( 1 );
                }
            } catch ( Exception e ) {
                Log.e( TAG, e.getMessage() );
            }
        }

        stashName.setText( stash.getDisplayText() );

        try {
            shareImage.setColorFilter( StylingUtilities.getColorFromText( context, stash.getShareBtn() ) );
        } catch ( NumberFormatException ignored ) {}
    }

    @OnClick( R2.id.share )
    public void onShareClick( View view ) {
        if ( listener != null ) {
            listener.onShareClick( stash, bitmap );
        }
    }

    private void setFooterAlphas( float alpha ) {
        inventoryPercentage.setAlpha( alpha );

        inventoryProgress.setAlpha( alpha );

        inventoryRemainingTitle.setAlpha( alpha );

        stashName.setAlpha( alpha );
    }

    public interface Listener {
        void onShareClick( Stash stash, Bitmap bitmap );
    }
}
