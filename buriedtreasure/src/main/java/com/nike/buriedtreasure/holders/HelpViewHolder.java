package com.nike.buriedtreasure.holders;

/*
 * Created by richardknowles on 6/19/17.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class HelpViewHolder extends RecyclerView.ViewHolder {

    public HelpViewHolder( View itemView ) {
        super( itemView );

        ButterKnife.bind( this, itemView );
    }
}
