package com.nike.buriedtreasure.holders;

/*
 * Created by richardknowles on 6/19/17.
 */

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.models.Background;
import com.nike.buriedtreasure.models.ColorHint;
import com.nike.buriedtreasure.models.Cover;
import com.nike.buriedtreasure.utilities.StylingUtilities;
import com.nike.buriedtreasure.utilities.Utility;
import com.nike.buriedtreasure.views.ICarouselItem;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LandingViewHolder extends RecyclerView.ViewHolder implements TextureView.SurfaceTextureListener {

    private static final String TAG = "LandingViewHolder";

    private boolean hasBackgroundImageLoaded;

    private MediaPlayer mediaPlayer;

    private Cover cover;

    private Listener listener;

    @BindView( R2.id.image )
    public ImageView backgroundImage;

    @BindView( R2.id.video_view )
    public TextureView videoView;

    @BindView( R2.id.title )
    public TextView title;

    @BindView( R2.id.price )
    public TextView price;

    @BindView( R2.id.description )
    public TextView description;

    public LandingViewHolder( View itemView, Listener listener ) {
        super( itemView );

        ButterKnife.bind( this, itemView );

        this.listener = listener;
    }

    public void bind( @NonNull ICarouselItem iCarouselItem ) {
        final Context context = backgroundImage.getContext();

        cover = (Cover) iCarouselItem;

        final Background background = cover.getBackground();

        if ( background != null ) {
            if ( !Utility.isNullOrEmpty( background.getVideoUrl() ) ) {
                videoView.setSurfaceTextureListener( this );
            } else if ( !Utility.isNullOrEmpty( background.getImageUrl() ) ) {
                displayBackgroundImage( background );
            }
        } else {
            hasBackgroundImageLoaded = true;

            checkImagesLoaded();
        }

        title.setText( cover.getHeader() );

        price.setText( cover.getSubheader() );

        description.setText( cover.getBody() );

        ColorHint colorHint = cover.getColorHint();

        if ( colorHint != null ) {
            try {
                int headerColor = StylingUtilities.getColorFromText( context, colorHint.getHeader() );

                int subHeaderColor = StylingUtilities.getColorFromText( context, colorHint.getSubheader() );

                int bodyColor =StylingUtilities.getColorFromText( context, colorHint.getBody() );

                title.setTextColor( headerColor );

                price.setTextColor( subHeaderColor );

                description.setTextColor( bodyColor );
            } catch ( NumberFormatException ignored ) {}
        }

        description.getViewTreeObserver().addOnPreDrawListener( new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                int maxLines = description.getHeight() / description.getLineHeight();

                description.setMaxLines( maxLines );

                description.getViewTreeObserver().removeOnPreDrawListener( this );

                return true;
            }
        } );

        itemView.getViewTreeObserver().addOnGlobalLayoutListener( new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if ( listener != null ) {
                    int[] locationsFrom = new int[ 2 ];

                    itemView.getLocationInWindow( locationsFrom );

                    int xStart = locationsFrom[ 0 ];

                    int yStart = locationsFrom[ 1 ];

                    listener.onLandingGlobalLayout( xStart, yStart, itemView.getWidth() );
                }

                itemView.getViewTreeObserver().removeOnGlobalLayoutListener( this );
            }
        } );
    }

    @Override
    public void onSurfaceTextureAvailable( SurfaceTexture surfaceTexture, int width, int height ) {
        Surface surface = new Surface( surfaceTexture );

        final Background background = cover.getBackground();

        if ( background != null ) {
            if ( !Utility.isNullOrEmpty( background.getVideoUrl() ) ) {
                videoView.setVisibility( View.VISIBLE );

                backgroundImage.setVisibility( View.GONE );

                Uri videoUri = Uri.parse( background.getVideoUrl() );

                try {
                    mediaPlayer = new MediaPlayer();

                    mediaPlayer.setDataSource( videoView.getContext(), videoUri );

                    mediaPlayer.setSurface( surface );

                    mediaPlayer.setLooping( true );

                    mediaPlayer.prepareAsync();

                    mediaPlayer.setOnPreparedListener( new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared( MediaPlayer mp ) {
                            mediaPlayer = mp;

                            mediaPlayer.start();

                            hasBackgroundImageLoaded = true;

                            checkImagesLoaded();
                        }
                    } );

                    mediaPlayer.setOnErrorListener( new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError( MediaPlayer mp, int what, int extra ) {
                            if ( !hasBackgroundImageLoaded ) {
                                displayBackgroundImage( background );
                            }

                            return true;
                        }
                    } );

                } catch ( Exception e ) {
                    Log.d( TAG, e.toString() );

                    displayBackgroundImage( background );
                }
            }
        }

    }

    @Override
    public void onSurfaceTextureSizeChanged( SurfaceTexture surface, int width, int height ) {}

    @Override
    public boolean onSurfaceTextureDestroyed( SurfaceTexture surface ) {
        if ( mediaPlayer != null ) {
            mediaPlayer.stop();

            mediaPlayer.release();

            mediaPlayer = null;
        }
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated( SurfaceTexture surface ) {

    }

    private void checkImagesLoaded() {
        if ( hasBackgroundImageLoaded && listener != null ) {
            listener.onLandingImagesLoaded();
        }
    }

    private void displayBackgroundImage( Background background ) {
        videoView.setVisibility( View.GONE );

        backgroundImage.setVisibility( View.VISIBLE );

        Glide.with( backgroundImage.getContext() )
                .load( background.getImageUrl() )
                .diskCacheStrategy( DiskCacheStrategy.ALL )
                .centerCrop()
                .crossFade()
                .into( new GlideDrawableImageViewTarget( backgroundImage ) {
                    @Override
                    protected void setResource( GlideDrawable resource ) {
                        super.setResource( resource );

                        hasBackgroundImageLoaded = true;

                        checkImagesLoaded();
                    }

                    @Override
                    public void onLoadFailed( Exception e, Drawable errorDrawable ) {
                        super.onLoadFailed( e, errorDrawable );

                        hasBackgroundImageLoaded = true;

                        checkImagesLoaded();
                    }
                } );
    }

    public interface Listener {
        void onLandingImagesLoaded();

        void onLandingGlobalLayout( float x, float y, int width );
    }
}
