package com.nike.buriedtreasure.constants;

/*
 * Created by richardknowles on 6/15/17.
 */

public final class GlobalConstants {

    public static final String BURIED_TREASURE_ONBOARDING_PREFS = "buried_treasure_onboarding";

    public static final String PHOTO_SPHERE_INSTRUCTION_PREFS = "photo_sphere_instruction";

    public static final String HUNT_DATA_PREFS = "hunt_data";

    public static final String UPMID_PREFS = "upmid_prefs";

    public static final String UP_MID_KEY = "upmid";

    public static final String HUNT_TYPE_TO_DISPLAY = "e37fe24a-1ae9-11e7-93ae-92361f002671";

    public static final int DEFAULT_UPDATE_INTERVAL = 30;

    public static final String THREAD_ID_EXTRA = "thread_id_extra";

    public static final String CAMPAIGN_ID_EXTRA = "campaign_id_extra";

    //TODO: Integration - remove these constants
    public static final String DEFAULT_UPMID = "3489170378";

    public static final String TEMP_AUTHTOKEN = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjE4YzNlZGNlLTUyMjItNDQ0MS05NzRhLWE4NTVlOWM5YTlkMXNpZyJ9.eyJpc3MiOiJvYXV0aDJhY2MiLCJpYXQiOjE1MDE2MDY4NTYsImxhdCI6MTUwMTYwNjU1NiwiZXhwIjoxNTE0NzA3MTk5LCJqdGkiOiI2MDExMzFkZC0yMDg3LTQ0OTgtOTk0Mi0yMjgyYjBkMjBlODciLCJhdWQiOlsiY29tLm5pa2UuZGlnaXRhbCJdLCJzdWIiOiJjb20ubmlrZS5jb21tZXJjZS5zbmtycy5pb3MiLCJzYnQiOiJuaWtlOmFkZCIsIm9yZyI6InBlcmYiLCJzY3AiOlsiY29tbWVyY2UiLCJuaWtlOmRpZ2l0YWwiXSwicHJuIjoiMTI5MDg4MzkwNTciLCJwcnQiOiJuaWtlOnBsdXMifQ.f0OmD7nQlH-MnPIRbcXiQDl2KEttymE8qCnvMExibZv29gIOO4OhJ6DcKpp5TYtjDG_A0RxBrGlo3E33GWMuqDyfFGDophkPOlkleUYP9okX_WjesfaZNmqICFnsTbjf2CMSHNDFFr7uDPCxU-0QcyQyoMJObmCC8d7_bWUpXd_oZ8z-ONW7uFRes0Nb-Gl-JDQR46di8wbQQJyz7LmbL287Elt9w7XB4EgTWnszxX1Hxy2ECv6XcL_vmYoD82fwPl8zackMZRh0rwjMpOK_r1DCFwcOW_MydfPoa3EBBDlZATdARhL0IzsFNWV_4INe4Oj3o12TiJHan6Cq5675Rw";

}
