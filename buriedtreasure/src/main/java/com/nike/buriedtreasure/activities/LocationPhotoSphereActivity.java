package com.nike.buriedtreasure.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.ColorInt;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.constants.GlobalConstants;
import com.nike.buriedtreasure.managers.BuriedTreasureManager;
import com.nike.buriedtreasure.models.Action;
import com.nike.buriedtreasure.models.ActionMetaData;
import com.nike.buriedtreasure.models.ColorHint;
import com.nike.buriedtreasure.models.Geo;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.models.HuntMetaData;
import com.nike.buriedtreasure.models.Locations;
import com.nike.buriedtreasure.models.ResolvePutBody;
import com.nike.buriedtreasure.models.ResolveResponse;
import com.nike.buriedtreasure.models.Stash;
import com.nike.buriedtreasure.utilities.HuntUtils;
import com.nike.buriedtreasure.utilities.NikeHttpClient;
import com.nike.buriedtreasure.utilities.StylingUtilities;
import com.nike.buriedtreasure.utilities.UIUtils;
import com.nike.buriedtreasure.utilities.Utility;
import com.nike.buriedtreasure.views.NetworkErrorView;

import java.lang.annotation.Retention;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/*
 * Created by richardknowles on 5/30/17.
 */

public class LocationPhotoSphereActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, NetworkErrorView.Listener {

    public static final String STASH_DATA_EXTRA = "stash_data_extra";

    private static final int PERMISSIONS_REQUEST_LOCATION = 100;

    private static final int REQUEST_CHECK_SETTINGS = 101;

    private static final String TAG = "LocationPhotoSphere";

    private static final int NO_ERROR = 0;

    private static final int BITMAP_LOAD_ERROR = 1;

    private static final int RESOLVE_HUNT_ERROR = 2;

    private static final int LOW_ACCURACY_INTERVAL = 500;

    private static final int LOOK_AROUND_DURATION = 4000;

    private static final int LOOK_AROUND_FADE_DURATION = 2000;

    private final CompositeSubscription compositeSubscription = new CompositeSubscription();

    private Hunt hunt;

    private Stash stash;

    private Subscription subscription;

    private GoogleApiClient googleApiClient;

    private LocationRequest fusedLocationApiRequest;

    private LocationListener fusedLocationApiLocationListener;

    private List<Location> pollingLocations;

    private LocationManager locationManager;

    private android.location.LocationListener locationListener;

    private CountDownTimer countDownTimer;

    private Location currentLocation;

    private BottomSheetBehavior bottomSheetBehavior;

    private AlertDialog alertDialog;

    private int updateInterval = GlobalConstants.DEFAULT_UPDATE_INTERVAL;

    private @NetworkErrorType int networkErrorType;

    private boolean isScanning;

    private boolean isPollingLocation;

    private boolean isGetItNow;

    private boolean hasRequestedPermission;

    private boolean hasFoundEm;

    @BindView( R2.id.pano_view )
    VrPanoramaView panoramaView;

    @BindView( R2.id.look_around )
    View lookAround;

    @BindView( R2.id.sphere_header )
    TextView header;

    @BindView( R2.id.sphere_subheader )
    TextView subHeader;

    @BindView( R2.id.footer_background )
    View containerBackground;

    @BindView( R2.id.im_here )
    TextView imHere;

    @BindView( R2.id.close )
    ImageView close;

    @BindView( R2.id.scanning_progress )
    ProgressBar scanningProgress;

    @BindView( R2.id.bottom_sheet )
    ViewGroup bottomSheet;

    @BindView( R2.id.title )
    TextView title;

    @BindView( R2.id.body )
    TextView body;

    @BindView( R2.id.see_locations )
    View seeLocations;

    @BindView( R2.id.progress_bar )
    ProgressBar progressBar;

    @BindView( R2.id.network_error )
    NetworkErrorView networkErrorView;

    @BindView( R2.id.footer_btn )
    View footer;

    @Retention(SOURCE)
    @IntDef( { RESOLVE_HUNT_ERROR, BITMAP_LOAD_ERROR, NO_ERROR } )
    private @interface NetworkErrorType {}

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_location_photo_sphere );

        ButterKnife.bind( this );

        panoramaView.setInfoButtonEnabled( false );

        panoramaView.setFullscreenButtonEnabled( false );

        panoramaView.setStereoModeButtonEnabled( false );

        panoramaView.setTouchTrackingEnabled( false );

        panoramaView.setEventListener( new VrPanoramaEventListener() {

            @Override
            public void onLoadSuccess() {
                super.onLoadSuccess();

                progressBar.setVisibility( View.GONE );
            }

            @Override
            public void onLoadError( String errorMessage ) {
                super.onLoadError( errorMessage );

                progressBar.setVisibility( View.GONE );
            }
        } );

        fusedLocationApiRequest = new LocationRequest();

        fusedLocationApiRequest.setInterval( LOW_ACCURACY_INTERVAL );

        fusedLocationApiRequest.setFastestInterval( LOW_ACCURACY_INTERVAL );

        fusedLocationApiRequest.setPriority( LocationRequest.PRIORITY_HIGH_ACCURACY );

        fusedLocationApiLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged( Location location ) {
                //Empty Listener.  This request is used to ensure updating the lastKnownLocation in the FusedLocationApi which will be used for polling
                Log.d( TAG, "fusedOnLocationChanged: " + currentLocation.toString() );
            }
        };

        locationManager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        locationListener = new android.location.LocationListener() {
            @Override
            public void onLocationChanged( Location location ) {
                if ( location != null ) {
                    currentLocation = location;

                    Log.d( TAG, "onLocationChanged: " + currentLocation.toString() );
                }
            }

            @Override
            public void onStatusChanged( String provider, int status, Bundle extras ) { }

            @Override
            public void onProviderEnabled( String provider ) { }

            @Override
            public void onProviderDisabled( String provider ) { }
        };

        googleApiClient = new GoogleApiClient.Builder( this )
                .addApi( LocationServices.API )
                .addConnectionCallbacks( this )
                .build();

        bottomSheetBehavior = BottomSheetBehavior.from( bottomSheet );

        bottomSheetBehavior.setState( BottomSheetBehavior.STATE_HIDDEN );

        bottomSheetBehavior.setBottomSheetCallback( new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged( @NonNull View bottomSheet, int newState ) {
                if ( newState == BottomSheetBehavior.STATE_DRAGGING ) {
                    bottomSheetBehavior.setState( BottomSheetBehavior.STATE_EXPANDED );
                }
            }

            @Override
            public void onSlide( @NonNull View bottomSheet, float slideOffset ) {}
        } );

        stash = getIntent().getParcelableExtra( STASH_DATA_EXTRA );

        hunt = getIntent().getParcelableExtra( StashActivity.HUNT_DATA_EXTRA );

        if ( stash != null ) {
            loadPanoImage( stash.getPhotoSphere() );

            updateInventory();

            progressBar.getIndeterminateDrawable().setColorFilter( StylingUtilities.getColorFromText( this, stash.getShareBtn() ),
                    PorterDuff.Mode.SRC_IN );

            if ( hunt != null ) {
                HuntMetaData huntMetaData = hunt.getHuntMetaData();

                SharedPreferences sharedPreferences = getSharedPreferences( GlobalConstants.PHOTO_SPHERE_INSTRUCTION_PREFS, Context.MODE_PRIVATE );

                boolean hasSeenInstructions = sharedPreferences.getBoolean( hunt.id, false );

                if ( hasSeenInstructions ) {
                    lookAround.setVisibility( View.GONE );
                } else {
                    lookAround.setVisibility( View.VISIBLE );

                    lookAround.postDelayed( new Runnable() {
                        @Override
                        public void run() {
                            lookAround.animate()
                                    .alpha( 0 )
                                    .setDuration( LOOK_AROUND_FADE_DURATION )
                                    .setListener( new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart( Animator animation ) {

                                        }

                                        @Override
                                        public void onAnimationEnd( Animator animation ) {
                                            lookAround.setVisibility( View.GONE );
                                        }

                                        @Override
                                        public void onAnimationCancel( Animator animation ) {

                                        }

                                        @Override
                                        public void onAnimationRepeat( Animator animation ) {

                                        }
                                    } );

                        }
                    }, LOOK_AROUND_DURATION );

                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putBoolean( hunt.id, true );

                    editor.apply();
                }

                if ( HuntUtils.getFoundStashes( hunt ).containsKey( hunt.getId() ) ) {
                    displayGetItNow();
                }

                if ( huntMetaData != null ) {
                    ColorHint colorHint = huntMetaData.getColorHint();

                    if ( colorHint != null ) {
                        try {
                            String closebuttonColorAsString = colorHint.getCloseBtn360();

                            if ( !Utility.isNullOrEmpty( closebuttonColorAsString ) ) {
                                close.setColorFilter( StylingUtilities.getColorFromText( this, closebuttonColorAsString ) );
                            }
                        } catch ( NumberFormatException ignored ) {}

                        header.setTextColor( getText360() );

                        updateInventory();
                    }

                    if ( !isGetItNow ) {
                        try {
                            updateInterval = Integer.parseInt( hunt.getHuntMetaData().getUpdateInterval() );
                        } catch ( NumberFormatException e ) {
                            Log.e( TAG, e.getMessage() );
                        }

                        subscribeHunt();
                    }
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        panoramaView.pauseRendering();

        stopLocationUpdate();

        if ( isPollingLocation ) {
            stopPollingLocation();

            isPollingLocation = true;
        }

        if ( googleApiClient != null && googleApiClient.isConnected() ) {
            googleApiClient.disconnect();
        }

        if ( countDownTimer != null ) {
            countDownTimer.cancel();
        }

        if ( subscription != null ) {
            subscription.unsubscribe();

            subscription = null;
        }

        super.onPause();
    }

    @Override
    protected void onResume() {
        panoramaView.resumeRendering();

        super.onResume();

        if ( !hasFoundEm ) {
            requestLocationUpdates();

            if ( isPollingLocation ) {
                startPollingLocation();
            }

            getHunt();

            if ( googleApiClient != null && !googleApiClient.isConnected() ) {
                googleApiClient.connect();
            }
        } else {
            stopScanningState();

            displayGetItNow();
        }

        if ( subscription == null || subscription.isUnsubscribed() ) {
            subscribeHunt();
        }
    }

    @Override
    protected void onDestroy() {
        panoramaView.shutdown();

        compositeSubscription.clear();

        if ( alertDialog != null && alertDialog.isShowing() ) {
            alertDialog.dismiss();
        }

        stopPollingLocation();

        stopLocationUpdate();

        if ( googleApiClient != null && googleApiClient.isConnected() ) {
            googleApiClient.disconnect();
        }

        if ( countDownTimer != null ) {
            countDownTimer.cancel();
        }

        super.onDestroy();
    }

    @OnClick( R2.id.close )
    public void onCloseClick( View view ) {
        finish();
    }

    @OnClick( R2.id.footer_btn )
    public void onFooterBtnClick( View view ) {
        if ( isGetItNow ) {
            footer.setEnabled( false );

            BuriedTreasureManager buriedTreasureManager = BuriedTreasureManager.getInstance( this );

            HashMap<String, ActionMetaData> foundStashes = HuntUtils.getFoundStashes( hunt );

            ActionMetaData actionMetaData = foundStashes.get( stash.getId() );

            startActivity( buriedTreasureManager.getProductThreadIntent( this, actionMetaData.getThreadId(), actionMetaData.getCampaignId() ) );
        } else {
            startScanning();
        }
    }

    @OnClick( R2.id.see_locations )
    public void onSeeLocationsClick( View view ) {
        finish();
    }

    @OnClick( R2.id.ok )
    public void onOkClick( View view ) {
        bottomSheetBehavior.setState( BottomSheetBehavior.STATE_HIDDEN );
    }

    @Override
    public void onRequestPermissionsResult( int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                if ( grantResults.length > 0 && grantResults[ 0 ] != PackageManager.PERMISSION_GRANTED ) {
                    alertDialog = UIUtils.getAllowLocationDialog( this ).show();
                } else if ( grantResults.length > 0 && grantResults[ 0 ] != PackageManager.PERMISSION_GRANTED ){
                    requestLocationUpdates();
                }
            }
        }
    }

    @Override
    public void onConnected( @Nullable Bundle bundle ) {
        requestLocationUpdates();

        if ( isPollingLocation ) {
            startPollingLocation();
        }
    }

    @Override
    public void onConnectionSuspended( int i ) {
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && hasFineLocation() ) {
            stopLocationUpdate();

            stopPollingLocation();
        }

        googleApiClient.connect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startScanning();

                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                break;
        }
    }

    @Override
    public void onNetworkCloseClick( View view ) {
        displayNetworkError( false, networkErrorType );
    }

    @Override
    public void onNetworkRetryClick( View view ) {
        switch ( networkErrorType ) {
            case BITMAP_LOAD_ERROR:
                if ( stash != null ) {
                    loadPanoImage( stash.getPhotoSphere() );
                }

                break;
            case RESOLVE_HUNT_ERROR:
                startScanning();

                break;
            case NO_ERROR:
                break;
        }

        displayNetworkError( false, networkErrorType );
    }

    private void subscribeHunt() {
        subscription = Observable.interval( updateInterval, TimeUnit.SECONDS )
                .flatMap( new Func1<Long, Observable<? extends Hunt>>() {
                    @Override
                    public Observable<? extends Hunt> call( Long n ) {
                        return NikeHttpClient.getApi().getHunt( HuntUtils.getUpMid( LocationPhotoSphereActivity.this ),
                                BuriedTreasureManager.getInstance( LocationPhotoSphereActivity.this ).getBearerAuthToken(), hunt.id );
                    }
                } )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribeOn( Schedulers.io() )
                .repeat()
                .subscribe( new Action1<Hunt>() {
                    @Override
                    public void call( Hunt currentHunt ) {
                        if ( currentHunt != null && currentHunt.getHuntMetaData() != null ) {
                            BuriedTreasureManager.getInstance( LocationPhotoSphereActivity.this ).saveHuntData( currentHunt );

                            HuntMetaData currentHuntMetaData = currentHunt.getHuntMetaData();

                            Locations locations = currentHuntMetaData.getLocations();

                            if ( !Utility.isNullOrEmpty( stash.getId() ) && locations != null && locations.getStashes() != null ) {
                                for ( Stash stashItem : locations.getStashes() ) {
                                    if ( stashItem != null && stash.getId().equals( stashItem.getId() ) ) {
                                        stash = stashItem;

                                        if ( !isScanning ) {
                                            updateInventory();
                                        }

                                        break;
                                    }
                                }
                            }

                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call( Throwable throwable ) {
                        Log.d( TAG, throwable.getMessage() );
                    }
                } );

        compositeSubscription.add( subscription );
    }

    private synchronized void loadPanoImage( String photoSphereUrl ) {
        progressBar.setVisibility( View.VISIBLE );

        final VrPanoramaView.Options viewOptions = new VrPanoramaView.Options();

        viewOptions.inputType = VrPanoramaView.Options.TYPE_MONO;

        Glide.with( this )
                .load( photoSphereUrl )
                .asBitmap()
                .diskCacheStrategy( DiskCacheStrategy.ALL )
                .into( new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady( Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation ) {
                        panoramaView.loadImageFromBitmap( resource, viewOptions );
                    }

                    @Override
                    public void onLoadFailed( Exception e, Drawable errorDrawable ) {
                        super.onLoadFailed( e, errorDrawable );

                        progressBar.setVisibility( View.GONE );

                        displayNetworkError( true, BITMAP_LOAD_ERROR );
                    }
                } );
    }

    private void startScanning() {
        if ( !isScanning ) {
            footer.setEnabled( false );

            if ( !hasFineLocation() ) {
                alertDialog = UIUtils.getAllowLocationDialog( this ).show();

                return;
            } else if ( !locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();

                builder.addLocationRequest( fusedLocationApiRequest );

                builder.setAlwaysShow( true );

                SettingsClient client = LocationServices.getSettingsClient( this );

                Task<LocationSettingsResponse> task = client.checkLocationSettings( builder.build() );

                task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        startScanning();
                    }
                });

                task.addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case CommonStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    ResolvableApiException resolvable = (ResolvableApiException) e;

                                    resolvable.startResolutionForResult( LocationPhotoSphereActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch ( IntentSender.SendIntentException ignored ) { }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                alertDialog = new AlertDialog.Builder( LocationPhotoSphereActivity.this )
                                        .setNegativeButton( android.R.string.cancel, null )
                                        .setPositiveButton( android.R.string.ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick( DialogInterface dialog, int which ) {
                                                startActivity( new Intent( android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS ) );
                                            }
                                        } )
                                        .setMessage( R.string.uses_location_to ).show();
                                break;
                        }
                    }
                });

                return;
            }

            isScanning = true;

            imHere.setVisibility( View.GONE );

            header.setText( getString( R.string.scanning ) );

            subHeader.setText( getString( R.string.locating_device ) );

            subHeader.setTextColor( getText360() );

            scanningProgress.setVisibility( View.VISIBLE );

            final int fromWidth = getResources().getDimensionPixelSize( R.dimen.im_here_button_width );

            final int toWidth = getResources().getDimensionPixelSize( R.dimen.im_here_button_height );

            ValueAnimator widthAnimation = ValueAnimator.ofInt( fromWidth, toWidth );

            widthAnimation.addUpdateListener( new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate( ValueAnimator animation ) {
                    Integer value = (Integer) animation.getAnimatedValue();

                    ViewGroup.LayoutParams layoutParams = containerBackground.getLayoutParams();

                    layoutParams.width = value;

                    containerBackground.setLayoutParams( layoutParams );
                }
            } );

            widthAnimation.addListener( new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart( Animator animation ) {

                }

                @Override
                public void onAnimationEnd( Animator animation ) {
                    if ( currentLocation != null && currentLocation.getAccuracy() > stash.getThreshold() ) {
                        resolveHunt( currentLocation );
                    } else {
                        startPollingLocation();
                    }
                }

                @Override
                public void onAnimationCancel( Animator animation ) {

                }

                @Override
                public void onAnimationRepeat( Animator animation ) {

                }
            } );

            widthAnimation.start();
        }
    }

    private void displayFailure( @NonNull ActionMetaData actionMetaData ) {
        title.setText( actionMetaData.getHeader() );

        body.setText( actionMetaData.getBody() );

        if ( hunt != null && hunt.getMetadata() != null ) {
            HuntMetaData huntMetaData = hunt.getHuntMetaData();

            if ( huntMetaData.getLocations() != null && huntMetaData.getLocations().getStashes() != null
                    && huntMetaData.getLocations().getStashes().size() > 1 ) {
                seeLocations.setVisibility( View.VISIBLE );
            } else {
                seeLocations.setVisibility( View.GONE );
            }
        }

        bottomSheetBehavior.setState( BottomSheetBehavior.STATE_EXPANDED );
    }

    private void displayNetworkError( boolean display, @NetworkErrorType int networkErrorType ) {
        if ( display ) {
            stopScanningState();

            networkErrorView.setVisibility( View.VISIBLE );

            this.networkErrorType = networkErrorType;
        } else {
            networkErrorView.setVisibility( View.GONE );

            this.networkErrorType = NO_ERROR;
        }

    }

    private void updateInventory() {
        if ( !isScanning && stash != null && !Utility.isNullOrEmpty( stash.getInventory() ) ) {
            subHeader.setText( getString( R.string.remaining_in_stash, stash.getInventory() ) );

            try {
                int inventoryPercent = Integer.parseInt( stash.getInventory() );

                Locations locations = hunt != null && hunt.getHuntMetaData() != null ? hunt.getHuntMetaData().getLocations() : null;

                if ( locations != null && inventoryPercent <= locations.getInventoryThreshold() ) {
                    subHeader.setTextColor( ContextCompat.getColor( this, R.color.red ) );
                } else {
                    subHeader.setTextColor( getText360() );
                }
            } catch ( NumberFormatException e ) {
                Log.e( TAG, e.getMessage() );
            }
        }
    }

    private @ColorInt int getText360() {
        try {
            if ( hunt != null && hunt.getHuntMetaData() != null ) {
                ColorHint colorHint = hunt.getHuntMetaData().getColorHint();

                if ( !Utility.isNullOrEmpty( colorHint.getText360() ) ) {
                    return StylingUtilities.getColorFromText( this, colorHint.getText360() );
                }
            }
        } catch( NumberFormatException ignored ){}

        return ContextCompat.getColor( this, android.R.color.white );
    }

    private void resolveHunt( @NonNull Location location ) {
        String lat = Double.toString( location.getLatitude() );

        String lon = Double.toString( location.getLongitude() );

        compositeSubscription.add( NikeHttpClient.getApi().resolveHunt( HuntUtils.getUpMid( LocationPhotoSphereActivity.this ),
                BuriedTreasureManager.getInstance( this ).getBearerAuthToken(), hunt.id, new ResolvePutBody( new Geo( lat, lon ),
                stash.getId(), hash( lat, lon ) ) )
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new Action1<ResolveResponse>() {
                    @Override
                    public void call( ResolveResponse resolveResponse ) {
                        if ( resolveResponse != null && !Utility.isNullOrEmpty( resolveResponse.getActions() )
                                && resolveResponse.getActions().get( 0 ) != null ) {
                            Action action = resolveResponse.getActions().get( 0 );

                            if ( action.getActionMetaData() != null ) {
                                if ( resolveResponse.isSuccess() ) {
                                    startActivity( new Intent( LocationPhotoSphereActivity.this, FoundEmActivity.class )
                                            .putExtra( FoundEmActivity.ACTION_METADATA_EXTRA, action.getActionMetaData() ) );

                                    hasFoundEm = true;
                                } else if ( !Utility.isNullOrEmpty( resolveResponse.getActions() ) ) {
                                    displayFailure( action.getActionMetaData() );

                                    stopScanningState();
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call( Throwable throwable ) {
                        isScanning = false;

                        stopScanningState();

                        Log.d( TAG, throwable.getMessage() );

                        displayNetworkError( true, RESOLVE_HUNT_ERROR );
                    }
                } ) );
    }

    private void getHunt() {
        compositeSubscription.add( NikeHttpClient.getApi().getHunt( HuntUtils.getUpMid( LocationPhotoSphereActivity.this ),
                BuriedTreasureManager.getInstance( this ).getBearerAuthToken(), hunt.id )
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new Action1<Hunt>() {
                    @Override
                    public void call( Hunt currentHunt ) {
                        if ( currentHunt != null && currentHunt.getHuntMetaData() != null ) {
                            BuriedTreasureManager.getInstance( LocationPhotoSphereActivity.this ).saveHuntData( currentHunt );

                            hunt = currentHunt;

                            HuntMetaData currentHuntMetaData = currentHunt.getHuntMetaData();

                            Locations locations = currentHuntMetaData.getLocations();

                            if ( !Utility.isNullOrEmpty( stash.getId() ) && locations != null && locations.getStashes() != null ) {
                                for ( Stash stashItem : locations.getStashes() ) {
                                    if ( stashItem != null && stash.getId().equals( stashItem.getId() ) ) {
                                        stash = stashItem;

                                        if ( !isScanning ) {
                                            updateInventory();
                                        }

                                        break;
                                    }
                                }
                            }

                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call( Throwable throwable ) {
                        Log.d( TAG, throwable.getMessage() );
                    }
                } ) );
    }

    private void requestLocationUpdates() {
        if ( !hasRequestedPermission && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            hasRequestedPermission = true;

            ActivityCompat.requestPermissions( this, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSIONS_REQUEST_LOCATION );
        } else if ( !isGetItNow && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED ) {
            if ( googleApiClient != null && googleApiClient.isConnected() ) {
                if ( currentLocation == null ) {
                    currentLocation = LocationServices.FusedLocationApi.getLastLocation( googleApiClient );
                }

                LocationServices.FusedLocationApi.requestLocationUpdates( googleApiClient, fusedLocationApiRequest, fusedLocationApiLocationListener );
            }

            locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, locationListener );
        }
    }

    private void startPollingLocation() {
        if ( ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED ) {
            isPollingLocation = true;

            if ( pollingLocations == null ) {
                pollingLocations = new ArrayList<>();

                pollingLocations.add( currentLocation );
            } else {
                pollingLocations.clear();
            }

            if ( countDownTimer == null ) {
                countDownTimer = new CountDownTimer( stash.getTimeout() * 1000, 500 ) {
                    @Override
                    public void onTick( long millisUntilFinished ) {
                        if ( ActivityCompat.checkSelfPermission( LocationPhotoSphereActivity.this, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED ) {
                            Location lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation( googleApiClient );

                            if ( lastKnownLocation != null ) {
                                pollingLocations.add( lastKnownLocation );

                                if ( lastKnownLocation.getAccuracy() > stash.getThreshold() ) {
                                    countDownTimer.cancel();

                                    stopPollingLocation();

                                    currentLocation = lastKnownLocation;

                                    resolveHunt( currentLocation );
                                }

                                Log.d( TAG, "onPollLocation: " +  currentLocation.toString() );
                            }
                        }
                    }

                    @Override
                    public void onFinish() {
                        stopPollingLocation();

                        if ( !Utility.isNullOrEmpty( pollingLocations ) ) {
                            Location bestLocation = pollingLocations.get( 0 );

                            for ( Location location : pollingLocations ) {
                                if ( location != null && location.getAccuracy() >= currentLocation.getAccuracy() ) {
                                    bestLocation = location;
                                }
                            }

                            currentLocation = bestLocation;

                            resolveHunt( bestLocation );
                        } else {
                            resolveHunt( currentLocation );
                        }
                    }
                };
            }

            countDownTimer.start();
        }
    }

    private void stopLocationUpdate() {
        if ( googleApiClient != null && googleApiClient.isConnected()
                && hasFineLocation() ) {
            LocationServices.FusedLocationApi.removeLocationUpdates( googleApiClient, fusedLocationApiLocationListener );

            locationManager.removeUpdates( locationListener );
        }
    }

    private void stopPollingLocation() {
        isPollingLocation = false;

        if ( countDownTimer != null ) {
            countDownTimer.cancel();
        }
    }

    private boolean hasFineLocation() {
        return ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED;
    }

    private String hash( String lat, String lon) {
        try {
            MessageDigest md5 = MessageDigest.getInstance( "MD5" );

            String hash = HuntUtils.getUpMid( LocationPhotoSphereActivity.this ) + "|"+ hunt.id + "|" + stash.getId() + "|" + lat + "|" + lon;

            md5.update(hash.getBytes(),0,hash.length());
            return new BigInteger(1,md5.digest()).toString(16);
        } catch (NoSuchAlgorithmException e ) {
            Log.e( TAG, e.getMessage() );
        }

        return null;
    }

    private void stopScanningState() {
        isScanning = false;

        footer.setEnabled( true );

        updateInventory();

        header.setText( getString( R.string.inventory_remaining ) );

        scanningProgress.setVisibility( View.GONE );

        imHere.setVisibility( View.VISIBLE );

        ViewGroup.LayoutParams layoutParams = containerBackground.getLayoutParams();

        layoutParams.width = getResources().getDimensionPixelSize( R.dimen.im_here_button_width );

        containerBackground.setLayoutParams( layoutParams );
    }

    private void displayGetItNow() {
        isGetItNow = true;

        imHere.setText( getString( R.string.get_it_now ) );

        subHeader.setVisibility( View.GONE );

        header.setVisibility( View.GONE );

        stopScanningState();
    }
}
