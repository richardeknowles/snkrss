package com.nike.buriedtreasure.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.constants.GlobalConstants;
import com.nike.buriedtreasure.fragments.EnableLocationFragment;
import com.nike.buriedtreasure.fragments.OnboardingFragment;
import com.nike.buriedtreasure.fragments.StashFragment;
import com.nike.buriedtreasure.managers.BuriedTreasureManager;
import com.nike.buriedtreasure.models.Cover;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.models.HuntMetaData;
import com.nike.buriedtreasure.models.Locations;
import com.nike.buriedtreasure.models.Stash;
import com.nike.buriedtreasure.tasks.ImageDownloaderTask;
import com.nike.buriedtreasure.utilities.HuntUtils;
import com.nike.buriedtreasure.utilities.NikeHttpClient;
import com.nike.buriedtreasure.utilities.UIUtils;
import com.nike.buriedtreasure.utilities.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class StashActivity extends BaseActivity implements OnboardingFragment.Listener,
        StashFragment.Listener, ImageDownloaderTask.Listener, EnableLocationFragment.Listener {

    public static final String TAG = "StashActivity";

    public static final String HUNT_DATA_EXTRA = "hunt_data_extra";

    private static final int PERMISSIONS_REQUEST_LOCATION = 1001;

    private final CompositeSubscription compositeSubscription = new CompositeSubscription(  );

    private Hunt hunt;

    private AlertDialog alertDialog;

    private StashFragment stashFragment;

    private Intent huntUpdatedIntent;

    private Subscription subscription;

    private int updateInterval = GlobalConstants.DEFAULT_UPDATE_INTERVAL;

    private boolean isImageDownloadComplete;

    private boolean hasLoadedOnboardingFromHelpCard;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_stash );

        ButterKnife.bind( this );

        hunt = getIntent().getParcelableExtra( StashActivity.HUNT_DATA_EXTRA );

        final HuntMetaData huntMetaData = hunt != null ? hunt.getHuntMetaData() : null;

        SharedPreferences sharedPreferences = getSharedPreferences( GlobalConstants.BURIED_TREASURE_ONBOARDING_PREFS, Context.MODE_PRIVATE );

        boolean hasSeenOnboarding = sharedPreferences.getBoolean( hunt.id, false );

        huntUpdatedIntent = new Intent( HuntUtils.HUNT_UPDATED_FILTER_ACTION );

        loadImages();

        addStashFragment();

        if ( !hasSeenOnboarding ) {
            addOnboardingFragment( false );

            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putBoolean( hunt.id, true );

            editor.apply();
        } else if ( ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions( this,
                    new String[]{ Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSIONS_REQUEST_LOCATION );
        }

        if ( hunt != null && huntMetaData != null ) {
            if ( !Utility.isNullOrEmpty( huntMetaData.getUpdateInterval() ) ) {
                try {
                    updateInterval = Integer.parseInt( huntMetaData.getUpdateInterval() );
                } catch ( NumberFormatException e ) {
                    Log.e( TAG, e.getMessage() );
                }

                subscribeHunt();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult( int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                if ( grantResults.length > 0 && grantResults[ 0 ] != PackageManager.PERMISSION_GRANTED ) {
                    alertDialog = UIUtils.getAllowLocationDialog( this ).show();
                }

                break;
            }
            default:
                super.onRequestPermissionsResult( requestCode, permissions, grantResults );
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        compositeSubscription.clear();

        subscription = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        refreshHunt();

        if ( subscription == null || subscription.isUnsubscribed() ) {
            subscribeHunt();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        compositeSubscription.clear();

        if ( alertDialog != null && alertDialog.isShowing() ) {
            alertDialog.dismiss();
        }
    }

    @Override
    public Hunt getHunt() {
        return hunt;
    }

    @Override
    public boolean isImageDownloadComplete() {
        return isImageDownloadComplete;
    }

    @Override
    public void onHelpCardClick() {
        hasLoadedOnboardingFromHelpCard = true;

        SharedPreferences sharedPreferences = getSharedPreferences( GlobalConstants.PHOTO_SPHERE_INSTRUCTION_PREFS, Context.MODE_PRIVATE );

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean( hunt.id, false );

        editor.apply();

        addOnboardingFragment( true );
    }

    @Override
    public void onSkipOrGetStartedClick( View view ) {
        Fragment onboardingFragment = getSupportFragmentManager().findFragmentByTag( OnboardingFragment.TAG );

        if ( onboardingFragment != null ) {
            getSupportFragmentManager().beginTransaction()
                    .remove( onboardingFragment )
                    .commitAllowingStateLoss();
        }

        if ( !hasLoadedOnboardingFromHelpCard ) {
            if ( ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
                    !ActivityCompat.shouldShowRequestPermissionRationale( this, Manifest.permission.ACCESS_FINE_LOCATION ) ) {
                getSupportFragmentManager().beginTransaction()
                        .add( R.id.container, EnableLocationFragment.newInstance(), EnableLocationFragment.TAG )
                        .addToBackStack( null )
                        .commitAllowingStateLoss();
            } else {
                addStashFragment();
            }
        }
    }

    @Override
    public void onResult( ImageDownloaderTask.Result result ) {
        isImageDownloadComplete = true;

        Intent imagesDownloadedIntent = new Intent( ImageDownloaderTask.IMAGES_DOWNLOADED_FILTER_ACTION );

        LocalBroadcastManager.getInstance( this ).sendBroadcast( imagesDownloadedIntent );
    }

    @Override
    public void navigateToCarousel() {
        addStashFragment();
    }

    private void subscribeHunt() {
        subscription = Observable.interval( updateInterval, TimeUnit.SECONDS )
                .flatMap( new Func1<Long, Observable<? extends Hunt>>() {
                    @Override
                    public Observable<? extends Hunt> call( Long n ) {
                        return NikeHttpClient.getApi().getHunt( HuntUtils.getUpMid( StashActivity.this ),
                                BuriedTreasureManager.getInstance( StashActivity.this ).getBearerAuthToken(), hunt.id );
                    }
                } )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribeOn( Schedulers.io() )
                .repeat()
                .subscribe( new Action1<Hunt>() {
                                @Override
                                public void call( Hunt currentHunt ) {
                                    if ( hunt != null ) {
                                        hunt = currentHunt;

                                        BuriedTreasureManager.getInstance( StashActivity.this ).saveHuntData( hunt );

                                        huntUpdatedIntent.putExtra( StashActivity.HUNT_DATA_EXTRA, currentHunt );

                                        LocalBroadcastManager.getInstance( StashActivity.this ).sendBroadcast( huntUpdatedIntent );
                                    }
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call( Throwable throwable ) {
                                    Log.e( TAG, throwable.getMessage() );
                                }
                            }
                );


        compositeSubscription.add( subscription );
    }

    private void loadImages() {
        List<String> imagesToDownload = new ArrayList<>();

        HuntMetaData huntMetaData = hunt != null ? hunt.getHuntMetaData() : null;

        if ( hunt != null && huntMetaData != null ) {
            if ( !Utility.isNullOrEmpty( huntMetaData.getBackgroundUrl() ) ) {
                imagesToDownload.add( huntMetaData.getBackgroundUrl() );
            }

            if ( huntMetaData.getCover() != null ) {
                Cover cover = huntMetaData.getCover();

                if ( cover.getBackground() != null && !Utility.isNullOrEmpty( cover.getBackground().getImageUrl() ) ) {
                    imagesToDownload.add( cover.getBackground().getImageUrl() );
                }

                if ( !Utility.isNullOrEmpty( cover.getImageUrl() ) ) {
                    imagesToDownload.add( cover.getImageUrl() );
                }
            }

            List<Stash> stashes = getStashes();

            if ( !Utility.isNullOrEmpty( stashes ) ) {
                for ( final Stash stash : stashes ) {
                    if ( stash != null ) {
                        imagesToDownload.add( stash.getImageUrl() );

                        imagesToDownload.add( stash.getPhotoSphere() );
                    }
                }
            }

            ImageDownloaderTask imageDownloaderTask = new ImageDownloaderTask( Glide.with( getApplicationContext() ), this );

            imageDownloaderTask.execute( imagesToDownload.toArray( new String[ imagesToDownload.size() ] ) );
        }
    }

    private void refreshHunt() {
        if ( hunt != null ) {
            compositeSubscription.add( NikeHttpClient.getApi().getHunt( HuntUtils.getUpMid( StashActivity.this ),
                    BuriedTreasureManager.getInstance( this ).getBearerAuthToken(), hunt.id )
                    .subscribeOn( Schedulers.io() )
                    .observeOn( AndroidSchedulers.mainThread() )
                    .subscribe( new Action1<Hunt>() {
                        @Override
                        public void call( Hunt currentHunt ) {
                            if ( currentHunt != null && currentHunt.getHuntMetaData() != null ) {
                                hunt = currentHunt;

                                BuriedTreasureManager.getInstance( StashActivity.this ).saveHuntData( hunt );

                                huntUpdatedIntent.putExtra( StashActivity.HUNT_DATA_EXTRA, currentHunt );

                                LocalBroadcastManager.getInstance( StashActivity.this ).sendBroadcast( huntUpdatedIntent );
                            }
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call( Throwable throwable ) {
                            Log.d( TAG, throwable.getMessage() );
                        }
                    } ) );
        }
    }

    private void addStashFragment() {
        if ( stashFragment == null ) {
            stashFragment = StashFragment.newInstance();
        }

        getSupportFragmentManager().beginTransaction()
                .replace( R.id.container, stashFragment, StashFragment.TAG )
                .commitAllowingStateLoss();
    }

    private void addOnboardingFragment( boolean addToBackStack ) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .add( R.id.container, OnboardingFragment.newInstance(), OnboardingFragment.TAG );

        if ( addToBackStack ) {
            transaction.addToBackStack( OnboardingFragment.TAG );
        }

        transaction.commitAllowingStateLoss();
    }

    private List<Stash> getStashes() {
        final HuntMetaData huntMetaData = hunt != null ? hunt.getHuntMetaData() : null;

        if ( huntMetaData != null && huntMetaData.getLocations() != null ) {
            Locations locations = huntMetaData.getLocations();

            return locations.getStashes();
        } else {
            return null;
        }
    }
}
