package com.nike.buriedtreasure.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

/*
 * Created by richardknowles on 6/29/17.
 */

@SuppressLint( "Registered" )
public class BaseActivity extends AppCompatActivity {

    private View decorView;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        decorView = getWindow().getDecorView();
    }

    @Override
    public void onWindowFocusChanged( boolean hasFocus ) {
        super.onWindowFocusChanged( hasFocus );

        if ( hasFocus ) {
            setImmersiveMode();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setImmersiveMode();
    }

    private void setImmersiveMode() {
        if ( decorView != null ) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY );
        }

        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
