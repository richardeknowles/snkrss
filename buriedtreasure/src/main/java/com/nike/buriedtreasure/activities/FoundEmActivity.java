package com.nike.buriedtreasure.activities;

/*
 * Created by richardknowles on 7/8/17.
 */

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.nike.buriedtreasure.R;
import com.nike.buriedtreasure.R2;
import com.nike.buriedtreasure.managers.BuriedTreasureManager;
import com.nike.buriedtreasure.models.ActionMetaData;
import com.nike.buriedtreasure.models.ColorHint;
import com.nike.buriedtreasure.utilities.StylingUtilities;
import com.nike.buriedtreasure.utilities.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FoundEmActivity extends BaseActivity {

    public static String ACTION_METADATA_EXTRA = "action_metadata_extra";

    private MediaPlayer mediaPlayer;

    private ActionMetaData actionMetaData;

    private boolean hasBackgroundImageLoaded;

    private boolean hasSneakerImageLoaded;

    @BindView(R2.id.video_view)
    VideoView videoView;

    @BindView(R2.id.background)
    ImageView background;

    @BindView(R2.id.sneaker_image)
    ImageView sneakerImage;

    @BindView( R2.id.header )
    TextView header;

    @BindView(R2.id.body)
    TextView body;

    @BindView( R2.id.get_it_now )
    TextView getItNow;

    @BindView( R2.id.progress )
    View loadingProgress;

    @BindView( R2.id.progress_bar )
    ProgressBar progressBar;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_found_em );

        ButterKnife.bind( this );

        actionMetaData = getIntent().getParcelableExtra( ACTION_METADATA_EXTRA );

        if ( actionMetaData != null ) {
            String videoUrl = actionMetaData.getBackground() != null ? actionMetaData.getBackground().getVideoUrl() : null;

            if ( !Utility.isNullOrEmpty( videoUrl ) ) {
                videoView.setVisibility( View.VISIBLE );

                background.setVisibility( View.GONE );

                Uri video = Uri.parse( videoUrl );

                videoView.setVideoURI( video );

                videoView.setOnPreparedListener( new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared( MediaPlayer mp ) {
                        mediaPlayer = mp;

                        mediaPlayer.setLooping( true );

                        mediaPlayer.start();

                        hasBackgroundImageLoaded = true;

                        checkImagesLoaded();
                    }
                } );

                videoView.setOnErrorListener( new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError( MediaPlayer mp, int what, int extra ) {
                        displayBackgroundImage( actionMetaData );

                        return true;
                    }
                } );
            } else {
                displayBackgroundImage( actionMetaData );
            }

            ColorHint colorHint = actionMetaData.getColorHint();

            if ( colorHint != null ) {
                header.setTextColor( StylingUtilities.getColorFromText( this, colorHint.getHeader() ) );

                body.setTextColor( StylingUtilities.getColorFromText( this, colorHint.getText() ) );

                getItNow.setTextColor( StylingUtilities.getColorFromText( this, colorHint.getButtonText() ) );

                @ColorInt int buttonColor = StylingUtilities.getColorFromText( this, colorHint.getButton() );

                GradientDrawable getItNowBackground = (GradientDrawable) getItNow.getBackground();

                getItNowBackground.setColor( buttonColor );

                progressBar.getIndeterminateDrawable().setColorFilter( StylingUtilities.getColorFromText( this, colorHint.getSpinner() ),
                        PorterDuff.Mode.SRC_IN );
            }

            Glide.with( this )
                    .load( actionMetaData.getImageUrl() )
                    .into( new GlideDrawableImageViewTarget( sneakerImage ) {

                        @Override
                        protected void setResource( GlideDrawable resource ) {
                            super.setResource( resource );

                            hasSneakerImageLoaded = true;

                            checkImagesLoaded();
                        }

                        @Override
                        public void onLoadFailed( Exception e, Drawable errorDrawable ) {
                            super.onLoadFailed( e, errorDrawable );

                            hasSneakerImageLoaded = true;

                            checkImagesLoaded();
                        }
                    } );

            header.setText( actionMetaData.getHeader() );

            body.setText( actionMetaData.getBody() );
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if ( videoView.isPlaying() ) {
            videoView.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if ( !videoView.isPlaying() ) {
            videoView.resume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if ( mediaPlayer != null ) {
            mediaPlayer.release();

            mediaPlayer = null;
        }
    }

    @OnClick(R2.id.get_it_now)
    public void onGetItNowClick( View view ) {
        getItNow.setEnabled( false ); //todo: manager

        BuriedTreasureManager buriedTreasureManager = BuriedTreasureManager.getInstance( this );

        startActivity( buriedTreasureManager.getProductThreadIntent( this, actionMetaData.getThreadId(), actionMetaData.getCampaignId() ) );
    }

    private void displayBackgroundImage( ActionMetaData actionMetaData ) {
        videoView.setVisibility( View.GONE );

        if ( actionMetaData.getBackground() != null ) {
            Glide.with( this )
                    .load( actionMetaData.getBackground().getImageUrl() )
                    .into( new GlideDrawableImageViewTarget( background ) {
                        @Override
                        protected void setResource( GlideDrawable resource ) {
                            super.setResource( resource );

                            hasBackgroundImageLoaded = true;

                            checkImagesLoaded();
                        }

                        @Override
                        public void onLoadFailed( Exception e, Drawable errorDrawable ) {
                            super.onLoadFailed( e, errorDrawable );

                            hasBackgroundImageLoaded = true;

                            checkImagesLoaded();
                        }
                    } );
        }

        hasBackgroundImageLoaded = true;

        checkImagesLoaded();
    }

    private void checkImagesLoaded() {
        if ( hasBackgroundImageLoaded && hasSneakerImageLoaded ) {
            loadingProgress.setVisibility( View.GONE );
        }
    }
}
