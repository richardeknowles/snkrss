package com.nike.buriedtreasure.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/*
 * Created by richardknowles on 6/21/17.
 */

public class HuntList implements Parcelable {

    @SerializedName("objects")
    private List<Hunt> hunts;

    private HuntList( Parcel in ) {
        hunts = in.createTypedArrayList( Hunt.CREATOR );
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeTypedList( hunts );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HuntList> CREATOR = new Creator<HuntList>() {
        @Override
        public HuntList createFromParcel( Parcel in ) {
            return new HuntList( in );
        }

        @Override
        public HuntList[] newArray( int size ) {
            return new HuntList[ size ];
        }
    };

    public List<Hunt> getHunts() {
        return hunts;
    }
}
