package com.nike.buriedtreasure.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/*
 * Created by richardknowles on 7/16/17.
 */

public class ResultMetaData implements Parcelable {

    @SerializedName( "resultData" )
    ResultData resultData;

    @SerializedName("completed")
    private long completed;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName( "hunt_id" )
    private String hunt_id;

    @SerializedName( "success" )
    private boolean success;


    protected ResultMetaData( Parcel in ) {
        resultData = in.readParcelable( ResultData.class.getClassLoader() );
        completed = in.readLong();
        user_id = in.readString();
        hunt_id = in.readString();
        success = in.readByte() != 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeParcelable( resultData, flags );
        dest.writeLong( completed );
        dest.writeString( user_id );
        dest.writeString( hunt_id );
        dest.writeByte( (byte) ( success ? 1 : 0 ) );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResultMetaData> CREATOR = new Creator<ResultMetaData>() {
        @Override
        public ResultMetaData createFromParcel( Parcel in ) {
            return new ResultMetaData( in );
        }

        @Override
        public ResultMetaData[] newArray( int size ) {
            return new ResultMetaData[ size ];
        }
    };

    public ResultData getResultData() {
        return resultData;
    }

    public long getCompleted() {
        return completed;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getHunt_id() {
        return hunt_id;
    }

    public boolean isSuccess() {
        return success;
    }
}
