package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 6/25/17.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Background implements Parcelable{

    @SerializedName( "imageUrl" )
    private String imageUrl;

    @SerializedName( "videoUrl" )
    private String videoUrl;

    private Background( Parcel in ) {
        imageUrl = in.readString();
        videoUrl = in.readString();
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString( imageUrl );
        dest.writeString( videoUrl );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Background> CREATOR = new Creator<Background>() {
        @Override
        public Background createFromParcel( Parcel in ) {
            return new Background( in );
        }

        @Override
        public Background[] newArray( int size ) {
            return new Background[ size ];
        }
    };

    public String getImageUrl() {
        return imageUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }
}
