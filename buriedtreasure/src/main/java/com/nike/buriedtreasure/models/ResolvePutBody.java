package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 6/16/17.
 */

public class ResolvePutBody {

    private Geo geo;

    private String stashId;

    private String hash;

    public ResolvePutBody( Geo geo, String stashId, String hash ) {
        this.geo = geo;

        this.stashId = stashId;

        this.hash = hash;
    }
}
