package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 7/8/17.
 */

import com.google.gson.annotations.SerializedName;

public class Geo {

    @SerializedName( "lat" )
    private String lat;

    @SerializedName( "lon" )
    private String lon;

    public Geo( String lat, String lon ) {
        this.lat = lat;
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }
}
