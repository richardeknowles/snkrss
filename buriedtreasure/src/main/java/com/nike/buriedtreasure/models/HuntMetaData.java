package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 6/25/17.
 */


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class HuntMetaData implements Parcelable {

    @SerializedName("invite")
    private String invite;

    @SerializedName("inviteUrl")
    private String inviteUrl;

    @SerializedName("cover")
    private Cover cover;

    @SerializedName("locations")
    private Locations locations;

    @SerializedName("threadId")
    private String threadId;

    @SerializedName("theme")
    private String theme;

    @SerializedName("detailsUrl")
    private String detailsUrl;

    @SerializedName("backgroundUrl")
    private String backgroundUrl;

    @SerializedName("updateInterval")
    private String updateInterval;

    @SerializedName("colorHint")
    private ColorHint colorHint;

    private HuntMetaData( Parcel in ) {
        invite = in.readString();
        inviteUrl = in.readString();
        threadId = in.readString();
        theme = in.readString();
        detailsUrl = in.readString();
        backgroundUrl = in.readString();
        updateInterval = in.readString();
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString( invite );
        dest.writeString( inviteUrl );
        dest.writeString( threadId );
        dest.writeString( theme );
        dest.writeString( detailsUrl );
        dest.writeString( backgroundUrl );
        dest.writeString( updateInterval );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HuntMetaData> CREATOR = new Creator<HuntMetaData>() {
        @Override
        public HuntMetaData createFromParcel( Parcel in ) {
            return new HuntMetaData( in );
        }

        @Override
        public HuntMetaData[] newArray( int size ) {
            return new HuntMetaData[ size ];
        }
    };

    public String getInvite() {
        return invite;
    }

    public String getInviteUrl() {
        return inviteUrl;
    }

    public Cover getCover() {
        return cover;
    }

    public Locations getLocations() {
        return locations;
    }

    public String getThreadId() {
        return threadId;
    }

    public String getTheme() {
        return theme;
    }

    public String getDetailsUrl() {
        return detailsUrl;
    }

    public String getBackgroundUrl() {
        return backgroundUrl;
    }

    public String getUpdateInterval() {
        return updateInterval;
    }

    public ColorHint getColorHint() {
        return colorHint;
    }
}
    
