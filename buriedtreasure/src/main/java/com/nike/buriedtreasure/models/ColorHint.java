package com.nike.buriedtreasure.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/*
 * Created by richardknowles on 6/25/17.
 */

public class ColorHint implements Parcelable{

    @SerializedName( "subheader" )
    private String subheader;

    @SerializedName( "header" )
    private String header;

    @SerializedName( "body" )
    private String body;

    @SerializedName( "theme" )
    private String theme;

    @SerializedName( "detailsBtn" )
    private String detailsBtn;

    @SerializedName( "360CloseBtn" )
    private String closeBtn360;

    @SerializedName( "360Text" )
    private String text360;

    @SerializedName("spinner")
    private String spinner;

    @SerializedName( "button" )
    private String button;

    @SerializedName("buttonText")
    private String buttonText;

    @SerializedName( "text" )
    private String text;

    private ColorHint( Parcel in ) {
        subheader = in.readString();
        header = in.readString();
        body = in.readString();
        theme = in.readString();
        detailsBtn = in.readString();
        closeBtn360 = in.readString();
        text360 = in.readString();
        spinner = in.readString();
        button = in.readString();
        buttonText = in.readString();
        text = in.readString();
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString( subheader );
        dest.writeString( header );
        dest.writeString( body );
        dest.writeString( theme );
        dest.writeString( detailsBtn );
        dest.writeString( closeBtn360 );
        dest.writeString( text360 );
        dest.writeString( spinner );
        dest.writeString( button );
        dest.writeString( buttonText );
        dest.writeString( text );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ColorHint> CREATOR = new Creator<ColorHint>() {
        @Override
        public ColorHint createFromParcel( Parcel in ) {
            return new ColorHint( in );
        }

        @Override
        public ColorHint[] newArray( int size ) {
            return new ColorHint[ size ];
        }
    };

    public String getSubheader() {
        return subheader;
    }

    public String getHeader() {
        return header;
    }

    public String getBody() {
        return body;
    }

    public String getTheme() {
        return theme;
    }

    public String getDetailsBtn() {
        return detailsBtn;
    }

    public String getCloseBtn360() {
        return closeBtn360;
    }

    public String getText360() {
        return text360;
    }

    public String getSpinner() {
        return spinner;
    }

    public String getButton() {
        return button;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getText() {
        return text;
    }
}
