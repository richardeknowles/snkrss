package com.nike.buriedtreasure.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/*
 * Created by richardknowles on 7/16/17.
 */

public class ResultData implements Parcelable {

    @SerializedName("success")
    private boolean success;

    @SerializedName("completed")
    private long completed;

    @SerializedName( "actions" )
    private List<Action> actions;

    @SerializedName("discovered")
    private String discovered;

    @SerializedName("trackingId")
    private String trackingId;

    @SerializedName("first")
    private boolean first;


    protected ResultData( Parcel in ) {
        success = in.readByte() != 0;
        completed = in.readLong();
        actions = in.createTypedArrayList( Action.CREATOR );
        discovered = in.readString();
        trackingId = in.readString();
        first = in.readByte() != 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeByte( (byte) ( success ? 1 : 0 ) );
        dest.writeLong( completed );
        dest.writeTypedList( actions );
        dest.writeString( discovered );
        dest.writeString( trackingId );
        dest.writeByte( (byte) ( first ? 1 : 0 ) );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResultData> CREATOR = new Creator<ResultData>() {
        @Override
        public ResultData createFromParcel( Parcel in ) {
            return new ResultData( in );
        }

        @Override
        public ResultData[] newArray( int size ) {
            return new ResultData[ size ];
        }
    };

    public boolean isSuccess() {
        return success;
    }

    public long getCompleted() {
        return completed;
    }

    public List<Action> getActions() {
        return actions;
    }

    public String getDiscovered() {
        return discovered;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public boolean isFirst() {
        return first;
    }
}
