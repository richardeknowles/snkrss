package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 6/25/17.
 */


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ActionMetaData implements Parcelable {

    @SerializedName( "body" )
    private String body;

    @SerializedName( "header" )
    private String header;

    @SerializedName( "stashId" )
    private String stashId;

    @SerializedName( "campaignId" )
    private String campaignId;

    @SerializedName( "imageUrl" )
    private String imageUrl;

    @SerializedName( "background" )
    private Background background;

    @SerializedName( "threadId" )
    private String threadId;

    @SerializedName( "colorHint" )
    private ColorHint colorHint;

    private ActionMetaData( Parcel in ) {
        body = in.readString();
        header = in.readString();
        stashId = in.readString();
        campaignId = in.readString();
        imageUrl = in.readString();
        background = in.readParcelable( Background.class.getClassLoader() );
        threadId = in.readString();
        colorHint = in.readParcelable( ColorHint.class.getClassLoader() );
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString( body );
        dest.writeString( header );
        dest.writeString( stashId );
        dest.writeString( campaignId );
        dest.writeString( imageUrl );
        dest.writeParcelable( background, flags );
        dest.writeString( threadId );
        dest.writeParcelable( colorHint, flags );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ActionMetaData> CREATOR = new Creator<ActionMetaData>() {
        @Override
        public ActionMetaData createFromParcel( Parcel in ) {
            return new ActionMetaData( in );
        }

        @Override
        public ActionMetaData[] newArray( int size ) {
            return new ActionMetaData[ size ];
        }
    };

    public String getBody() {
        return body;
    }

    public String getHeader() {
        return header;
    }

    public String getStashId() {
        return stashId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Background getBackground() {
        return background;
    }

    public String getThreadId() {
        return threadId;
    }

    public ColorHint getColorHint() {
        return colorHint;
    }
}
    
