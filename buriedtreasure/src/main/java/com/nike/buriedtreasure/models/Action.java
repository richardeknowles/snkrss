package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 6/16/17.
 */

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;

public class Action implements Parcelable {

    private String type;

    private String metadata;

    private ActionMetaData actionMetaData;

    private Action( Parcel in ) {
        type = in.readString();
        metadata = in.readString();
        actionMetaData = in.readParcelable( ActionMetaData.class.getClassLoader() );
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString( type );
        dest.writeString( metadata );
        dest.writeParcelable( actionMetaData, flags );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Action> CREATOR = new Creator<Action>() {
        @Override
        public Action createFromParcel( Parcel in ) {
            return new Action( in );
        }

        @Override
        public Action[] newArray( int size ) {
            return new Action[ size ];
        }
    };

    public String getType() {
        return type;
    }

    public String getMetadata() {
        return metadata;
    }

    public ActionMetaData getActionMetaData() {
        if ( actionMetaData == null ) {
            try {
                Gson gson = new Gson();

                actionMetaData = gson.fromJson( new String( Base64.decode( metadata, Base64.DEFAULT ) ), ActionMetaData.class );
            } catch ( Exception e ) {
                Log.e( "Action", e.getMessage() );
            }
        }

        return actionMetaData;
    }
}
