package com.nike.buriedtreasure.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/*
 * Created by richardknowles on 6/25/17.
 */

public class Locations implements Parcelable {

    @SerializedName( "stashes" )
    private List<Stash> stashes;

    @SerializedName( "inventoryThreshold" )
    private int inventoryThreshold;

    private Locations( Parcel in ) {
        stashes = in.createTypedArrayList( Stash.CREATOR );
        inventoryThreshold = in.readInt();
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeTypedList( stashes );
        dest.writeInt( inventoryThreshold );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Locations> CREATOR = new Creator<Locations>() {
        @Override
        public Locations createFromParcel( Parcel in ) {
            return new Locations( in );
        }

        @Override
        public Locations[] newArray( int size ) {
            return new Locations[ size ];
        }
    };

    public List<Stash> getStashes() {
        return stashes;
    }

    public double getInventoryThreshold() {
        return inventoryThreshold;
    }
}
