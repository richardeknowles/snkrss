package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 6/25/17.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.nike.buriedtreasure.views.ICarouselItem;

public class Stash implements Parcelable, ICarouselItem {

    @SerializedName( "imageUrl" )
    private String imageUrl;

    @SerializedName( "threshold" )
    private double threshold;

    @SerializedName( "displayText" )
    private String displayText;

    @SerializedName( "inventory" )
    private String inventory;

    @SerializedName( "timeout" )
    private int timeout;

    @SerializedName( "photoSphere" )
    private String photoSphere;

    @SerializedName( "shareBtn" )
    private String shareBtn;

    @SerializedName( "id" )
    private String id;

    private boolean hasBeenFound;

    protected Stash( Parcel in ) {
        imageUrl = in.readString();
        threshold = in.readDouble();
        displayText = in.readString();
        inventory = in.readString();
        timeout = in.readInt();
        photoSphere = in.readString();
        shareBtn = in.readString();
        id = in.readString();
        hasBeenFound = in.readByte() != 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString( imageUrl );
        dest.writeDouble( threshold );
        dest.writeString( displayText );
        dest.writeString( inventory );
        dest.writeInt( timeout );
        dest.writeString( photoSphere );
        dest.writeString( shareBtn );
        dest.writeString( id );
        dest.writeByte( (byte) ( hasBeenFound ? 1 : 0 ) );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Stash> CREATOR = new Creator<Stash>() {
        @Override
        public Stash createFromParcel( Parcel in ) {
            return new Stash( in );
        }

        @Override
        public Stash[] newArray( int size ) {
            return new Stash[ size ];
        }
    };

    public String getImageUrl() {
        return imageUrl;
    }

    public double getThreshold() {
        return threshold;
    }

    public String getDisplayText() {
        return displayText;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory( String inventory ) {
        this.inventory = inventory;
    }

    public int getTimeout() {
        return timeout;
    }

    public String getPhotoSphere() {
        return photoSphere;
    }

    public String getShareBtn() {
        return shareBtn;
    }

    public String getId() {
        return id;
    }

    public boolean isHasBeenFound() {
        return hasBeenFound;
    }

    public void setHasBeenFound( boolean hasBeenFound ) {
        this.hasBeenFound = hasBeenFound;
    }

    @Override
    public int getType() {
        return ICarouselItem.LOCATION;
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) return true;
        if ( !(o instanceof Stash) ) return false;

        Stash that = (Stash) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
