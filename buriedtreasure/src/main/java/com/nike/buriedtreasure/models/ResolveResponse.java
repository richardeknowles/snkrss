package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 6/16/17.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResolveResponse implements Parcelable {

    @SerializedName( "discovered" )
    private String discovered;

    @SerializedName( "completed" )
    private String completed;

    @SerializedName( "actions" )
    private List<Action> actions;

    @SerializedName( "success" )
    private boolean success;

    @SerializedName( "trackingId" )
    private String trackingId;

    @SerializedName( "first" )
    private boolean first;

    private ResolveResponse( Parcel in ) {
        discovered = in.readString();
        completed = in.readString();
        actions = in.createTypedArrayList( Action.CREATOR );
        success = in.readByte() != 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString( discovered );
        dest.writeString( completed );
        dest.writeTypedList( actions );
        dest.writeByte( (byte) ( success ? 1 : 0 ) );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResolveResponse> CREATOR = new Creator<ResolveResponse>() {
        @Override
        public ResolveResponse createFromParcel( Parcel in ) {
            return new ResolveResponse( in );
        }

        @Override
        public ResolveResponse[] newArray( int size ) {
            return new ResolveResponse[ size ];
        }
    };

    public String getDiscovered() {
        return discovered;
    }

    public String getCompleted() {
        return completed;
    }

    public List<Action> getActions() {
        return actions;
    }

    public boolean isSuccess() {
        return success;
    }
}
