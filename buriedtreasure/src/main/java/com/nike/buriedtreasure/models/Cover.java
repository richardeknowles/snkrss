package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 6/25/17.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.nike.buriedtreasure.views.ICarouselItem;

public class Cover implements Parcelable, ICarouselItem {

    @SerializedName( "subheader" )
    private String subheader;

    @SerializedName( "body" )
    private String body;

    @SerializedName( "imageUrl" )
    private String imageUrl;

    @SerializedName( "header" )
    private String header;

    @SerializedName( "background" )
    private Background background;

    @SerializedName( "colorHint" )
    private ColorHint colorHint;

    private Cover( Parcel in ) {
        subheader = in.readString();
        body = in.readString();
        imageUrl = in.readString();
        header = in.readString();
        background = in.readParcelable( Background.class.getClassLoader() );
        colorHint = in.readParcelable( ColorHint.class.getClassLoader() );
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString( subheader );
        dest.writeString( body );
        dest.writeString( imageUrl );
        dest.writeString( header );
        dest.writeParcelable( background, flags );
        dest.writeParcelable( colorHint, flags );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Cover> CREATOR = new Creator<Cover>() {
        @Override
        public Cover createFromParcel( Parcel in ) {
            return new Cover( in );
        }

        @Override
        public Cover[] newArray( int size ) {
            return new Cover[ size ];
        }
    };

    public String getSubheader() {
        return subheader;
    }

    public String getBody() {
        return body;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getHeader() {
        return header;
    }

    public Background getBackground() {
        return background;
    }

    public ColorHint getColorHint() {
        return colorHint;
    }

    @Override
    public int getType() {
        return ICarouselItem.LANDING;
    }
}
