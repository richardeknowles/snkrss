package com.nike.buriedtreasure.models;

/*
 * Created by richardknowles on 6/16/17.
 */

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Hunt implements Parcelable {

    @SerializedName( "discovered" )
    private String discovered;

    @SerializedName( "valid" )
    private String valid;

    @SerializedName( "success" )
    private boolean success;

    @SerializedName( "resultData" )
    private String resultData;

    @SerializedName( "completed" )
    private String completed;

    @SerializedName( "type" )
    public String type;

    @SerializedName( "id" )
    public String id;

    @SerializedName( "metadata" )
    private String metadata;

    @SerializedName( "huntMetaData" )
    private HuntMetaData huntMetaData;

    @SerializedName( "resultMetaData" )
    private ResultMetaData resultMetaData;

    protected Hunt( Parcel in ) {
        discovered = in.readString();
        valid = in.readString();
        success = in.readByte() != 0;
        resultData = in.readString();
        completed = in.readString();
        type = in.readString();
        id = in.readString();
        metadata = in.readString();
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString( discovered );
        dest.writeString( valid );
        dest.writeByte( (byte) ( success ? 1 : 0 ) );
        dest.writeString( resultData );
        dest.writeString( completed );
        dest.writeString( type );
        dest.writeString( id );
        dest.writeString( metadata );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Hunt> CREATOR = new Creator<Hunt>() {
        @Override
        public Hunt createFromParcel( Parcel in ) {
            return new Hunt( in );
        }

        @Override
        public Hunt[] newArray( int size ) {
            return new Hunt[ size ];
        }
    };

    public String getDiscovered() {
        return discovered;
    }

    public String getValid() {
        return valid;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getResultData() {
        return resultData;
    }

    public String getCompleted() {
        return completed;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getMetadata() {
        return metadata;
    }

    public HuntMetaData getHuntMetaData() {
        if ( huntMetaData == null ) {
            try {
                Gson gson = new Gson();

                huntMetaData = gson.fromJson( new String( Base64.decode( metadata, Base64.DEFAULT ) ), HuntMetaData.class );
            } catch ( Exception e ) {
                Log.e( "Hunt", e.getMessage() );
            }
        }

        return huntMetaData;
    }

    public ResultMetaData getResultMetaData() {
        if ( resultMetaData == null ) {
            try {
                Gson gson = new Gson();

                if ( resultData != null ) {
                    resultMetaData = gson.fromJson( new String( Base64.decode( resultData, Base64.DEFAULT ) ), ResultMetaData.class );
                }
            } catch ( Exception e ) {
                Log.e( "Hunt", e.getMessage() );
            }
        }

        return resultMetaData;
    }
}
