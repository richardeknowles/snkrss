package com.nike.buriedtreasure.utilities;

/*
 * Created by richardknowles on 7/19/17.
 */

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.nike.buriedtreasure.interfaces.HuntAPI;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NikeHttpClient {

    public static  @NonNull HuntAPI getApi() {
        final Gson gson = new Gson();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor( new HttpLoggingInterceptor().setLevel( HttpLoggingInterceptor.Level.BODY ) );

        httpClient.addInterceptor( new Interceptor() {
            @Override
            public Response intercept( Chain chain ) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("X-NIKE-DEBUG-ENVIRONMENT", HuntAPI.DEBUG_ENVIRONMENT)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        } );

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl( HuntAPI.BASE_URL )
                .addCallAdapterFactory( RxJavaCallAdapterFactory.create() )
                .addConverterFactory( GsonConverterFactory.create( gson ) )
                .client( httpClient.build() )
                .build();

        return retrofit.create( HuntAPI.class );
    }

}