package com.nike.buriedtreasure.utilities;

/*
 * Created by richardknowles on 6/15/17.
 */

import java.util.List;

public final class Utility {

    public static boolean isNullOrEmpty( String string ) {
        return string == null || string.isEmpty();
    }

    public static boolean isNullOrEmpty( List list ) {
        return list == null || list.isEmpty();
    }
}
