package com.nike.buriedtreasure.utilities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cpaian on 9/3/15, modified by Richard Knowles
 */

public class StylingUtilities {

    /*
    * Caches typefaces based on their file path and name, so that they don't have to be created
    * every time when they are referenced.
    */
    private static Map<String, Typeface> sTypefaceCache = null;

    /**
     * Gets a Typeface resource from cache or via Asset Manager.
     *
     * @param path The file name of the font data in the assets directory (e.g., "fonts/HelveticaNeueW01-55Roman.ttf")
     * @return the Typeface object
     */
    @NonNull
    public static Typeface getTypefaceFromAsset( @NonNull Context context, @NonNull String path ) {
        if ( sTypefaceCache == null ) {
            sTypefaceCache = new HashMap<>();
        }
        Typeface typeface = sTypefaceCache.get( path );
        if ( typeface == null ) {
            typeface = Typeface.createFromAsset( context.getAssets(), path );
            sTypefaceCache.put( path, typeface );
        }
        return typeface;
    }


    public static @ColorInt int getColorFromText( @NonNull Context context, String colorText ) {
        if ( !Utility.isNullOrEmpty( colorText ) ) {
            try {
                return Color.parseColor( colorText.contains( "#" )
                        ? colorText : "#" + colorText );
            } catch ( NumberFormatException ignored ) {}
        }

        return ContextCompat.getColor( context, android.R.color.black );
    }

}
