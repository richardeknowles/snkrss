package com.nike.buriedtreasure.utilities;

/*
 * Created by richardknowles on 6/15/17.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.nike.buriedtreasure.constants.GlobalConstants;
import com.nike.buriedtreasure.models.Action;
import com.nike.buriedtreasure.models.ActionMetaData;
import com.nike.buriedtreasure.models.Hunt;
import com.nike.buriedtreasure.models.ResultData;
import com.nike.buriedtreasure.models.ResultMetaData;

import java.util.HashMap;
import java.util.List;

public final class HuntUtils {

    public static final String HUNT_UPDATED_FILTER_ACTION = "com.nike.buriedtreasure.HUNT_UPDATED";

    public static @NonNull HashMap<String, ActionMetaData> getFoundStashes( @NonNull Hunt hunt ) {
        HashMap<String, ActionMetaData> foundStashes = new HashMap<>();

        ResultMetaData resultMetaData = hunt.getResultMetaData();

        if ( resultMetaData != null ) {
            ResultData resultData = resultMetaData.getResultData();

            if ( resultData != null && resultData.isSuccess() ) {
                List<Action> actions = resultData.getActions();

                if ( !Utility.isNullOrEmpty( actions ) ) {
                    for ( Action action : actions ) {
                        ActionMetaData actionMetaData = action != null ? action.getActionMetaData() : null;
                        if ( actionMetaData != null ) {
                            foundStashes.put( actionMetaData.getStashId(), actionMetaData );
                        }
                    }
                }
            }
        }

        return foundStashes;
    }

    public static String getUpMid( @NonNull Context context ) {
        SharedPreferences sharedPreferences = context.getSharedPreferences( GlobalConstants.UPMID_PREFS, Context.MODE_PRIVATE );

        return sharedPreferences.getString( GlobalConstants.UP_MID_KEY, GlobalConstants.DEFAULT_UPMID );
    }

}
