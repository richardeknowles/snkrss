package com.nike.buriedtreasure.utilities;

/*
 * Created by richardknowles on 6/15/17.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.nike.buriedtreasure.R;

public final class UIUtils {

    public static AlertDialog.Builder getAllowLocationDialog( final Context context ) {
        return new AlertDialog.Builder( context )
                .setNegativeButton( android.R.string.ok, null )
                .setPositiveButton( R.string.open_settings, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick( DialogInterface dialog, int which ) {
                        context.startActivity( new Intent().setAction( Settings.ACTION_APPLICATION_DETAILS_SETTINGS )
                                .setData( Uri.fromParts( "package", context.getPackageName(), null ) ) );
                    }
                } )
                .setMessage( R.string.uses_location_to );
    }
}
